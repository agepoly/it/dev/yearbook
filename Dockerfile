FROM debian:bookworm AS runtime
RUN apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates openssl curl && ldconfig /usr/local/lib64/

FROM rust:1.79 AS rust_base
RUN cargo install cargo-chef --version ^0.1
WORKDIR /app

FROM rust_base AS rust_planner
COPY backend/ ./
RUN cargo chef prepare --recipe-path recipe.json

FROM rust_base AS rust_build
COPY --from=rust_planner /app/recipe.json recipe.json
# Build dependencies - this is the caching Docker layer!
RUN cargo chef cook --release --recipe-path recipe.json
# Build application

COPY backend/ ./
RUN cargo run --bin prisma -- generate
RUN cargo build --release --bin yearbook

FROM node:latest AS node_build
WORKDIR /node
COPY frontend/package*.json ./
RUN npm i
COPY frontend/ ./
RUN npm run build

FROM runtime
WORKDIR /app
COPY backend/Rocket.toml ./Rocket.toml
COPY --from=rust_build /app/target/release/yearbook ./yearbook
COPY --from=node_build /node/dist ./static
CMD ["./yearbook"]
