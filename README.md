# Yearbook by AGEPoly

The goal is to ease collection of the Yearbook data at the end of a year.

NB: [specifc information related to object storage](./rclone.md)

## Current configuration

Parameter | Value | Note
----------|-------|------
Max upload size | 10 MiB | Enforced by Rocket config, checked by frontend
Accepted image formats | JPEG & PNG | File extension checked during backend decoding
Final profile dimensions | 420x522 | Size of the generated preview, min size enforced by backend

# Dev: Frontend

Made in Typescript with Vue, Vuetify and TailwindCSS

In `/frontend` folder
```
## Setup
npm install

### Hot-reloads for development
npm run dev
```

# Dev: Backend

Made in Rust with Rocket and SQLx

## Development setup

### Database
How to use the SQLx database driver: https://wiki2.agepoly.ch/fr/comite-de-direction/it/dev/guides#sqlx-driver-db

In short:
- After a pull: `cargo sqlx migrate run`
- Before any commit: `cargo sqlx prepare` 

And `cargo sqlx migrate add <name>` to create a new migration (not reversible for this project).

### HTTP API Server

Simply `cargo c` for compilation check and `cargo r` to start the server.

# Continuous Integration

At the top-level directory there is a Dockerfile that will build both the frontend and the backend and prepare a single Docker image to deploy.

The Gitlab CI builds the image after each push to main branch with tags latest and commit hash prefix. 