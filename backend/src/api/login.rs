use crate::utilities::{
    auth::{self, AuthenticationError},
    database::DatabaseError,
    mailing::{self, MailingError},
    models::{LogType, ReportType, YearbookApiError, YearbookApiResponse},
};
use rocket::{futures::TryFutureExt, serde::json::Json, Route};
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_form_open,
        post_tequila_request,
        post_tequila_callback,
        post_email_login_request,
        post_email_login_callback
    ]
}

/// Check if the form is open
#[utoipa::path(
    responses(
        (status = 200, body =  bool),
        (status = 403, body = YearbookApiError),
    )
)]
#[get("/api/login/form-open")]
async fn get_form_open() -> YearbookApiResponse<bool> {
    Ok(Json(crate::config().form_open))
}

#[derive(ToSchema, Serialize)]
pub struct PostTequilaRequestResponse {
    auth_url: String,
}

/// Tequila Request
///
/// Get Tequila authentication URL for the client redirection
#[utoipa::path(
    responses(
        (status = 200, body = PostTequilaRequestResponse),
        (status = 500, body = YearbookApiError)
    ),
    security()
)]
#[post("/api/login/tequila-request", format = "json")]
pub async fn post_tequila_request() -> YearbookApiResponse<PostTequilaRequestResponse> {
    let auth_url = auth::new_tequila_request()
        .await
        .map_err(|_err| YearbookApiError::InternalError.res())?;
    Ok(Json(PostTequilaRequestResponse { auth_url }))
}

#[derive(ToSchema, Deserialize)]
pub struct PostTequilaCallbackForm {
    request_key: String,
    auth_check: String,
}

#[derive(ToSchema, Serialize)]
pub struct PostTequilaCallbackResponse {
    session_key: String,
}

/// Tequila Callback
///
/// To be used after Tequila redirects to the app. Returns the session_key
#[utoipa::path(
    request_body = PostTequilaCallbackForm,
    responses(
        (status = 200, body = PostTequilaCallbackResponse),
        (status = 400, body = YearbookApiError),
        (status = 404, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
    security()
)]
#[post(
    "/api/login/tequila-callback",
    format = "json",
    data = "<tequila_form>"
)]
pub async fn post_tequila_callback(
    tequila_form: Json<PostTequilaCallbackForm>,
    db: &crate::DB,
) -> YearbookApiResponse<PostTequilaCallbackResponse> {
    let user = auth::tequila_callback(db, &tequila_form.request_key, &tequila_form.auth_check)
        .await
        .map_err(|err| {
            match err {
                AuthenticationError::NotFound => YearbookApiError::NotFound,
                AuthenticationError::TequilaUserError => YearbookApiError::ProtocolError,
                AuthenticationError::InternalError => YearbookApiError::InternalError,
            }
            .res()
        })?;

    let session_key = user.session_key.ok_or_else(|| {
        error!("tequila_callback: missing session_key");
        YearbookApiError::InternalError.res()
    })?;
    Ok(Json(PostTequilaCallbackResponse { session_key }))
}

#[derive(ToSchema, Deserialize)]
pub struct PostEmailLoginRequest {
    sciper: String,
    email: String,
    ignore_email_warning: bool,
}

#[derive(ToSchema, Serialize)]
pub struct PostEmailLoginResponse {
    /// Sciper is not on the list of expected users : blocking issue
    warning_list: bool,
    /// Email is not on the list but sciper ok, may be overriden by user
    warning_email: bool,
}

/// Email Login Request
///
/// Returns no warning if the email has been sent. Performs multiple checks to determine if the user is elligible to email login (on the list, email matching)
#[utoipa::path(
    request_body = PostEmailLoginRequest,
    responses(
        (status = 200, body = PostEmailLoginResponse),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
    security()
)]
#[post("/api/login/email-login-request", format = "json", data = "<form>")]
pub async fn post_email_login_request(
    form: Json<PostEmailLoginRequest>,
    db: &crate::DB,
) -> YearbookApiResponse<PostEmailLoginResponse> {
    let (official_firstname, official_lastname, official_program) =
        match db.get_expected_user(form.sciper.clone()).await {
            Ok(eu) => {
                // check if the mail matches the expected one or if the client overrides
                if !form.ignore_email_warning && !eu.mail_perso.contains(&form.email) {
                    return Ok(Json(PostEmailLoginResponse {
                        warning_email: true,
                        warning_list: false,
                    }));
                }
                (eu.firstname, eu.lastname, eu.program)
            }
            Err(err) => match err {
                DatabaseError::NotFound => {
                    db.log(
                        LogType::UserWarn,
                        format!(
                            "List warning for sciper={} email={}",
                            form.sciper, form.email
                        ),
                        None,
                    )
                    .await;
                    return Ok(Json(PostEmailLoginResponse {
                        warning_email: false,
                        warning_list: true,
                    }));
                }
                _ => return Err(YearbookApiError::InternalError.res()),
            },
        };

    let new_token = auth::generate_token();
    match db
        .get_user(form.sciper.clone())
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?
    {
        Some(user) => {
            // user already registered

            let email = match user.email {
                Some(email) => email,
                None => {
                    error!("post_email_login_request: email login requested on Tequila account (email=None)");
                    db.log(
                        LogType::UserWarn,
                        format!(
                            "Email login requested on Tequila account sciper={} email={}",
                            form.sciper.clone(),
                            form.email.clone()
                        ),
                        Some(form.sciper.clone()),
                    )
                    .await;
                    return Err(YearbookApiError::Conflict.res());
                }
            };

            // check for email conflict
            if user.identity_verified && email != form.email {
                error!(
                    "post_email_login_request: email login requested with different email {} != {}",
                    form.email, email
                );
                db.log(
                    LogType::UserWarn,
                    format!(
                        "Email login requested with different email sciper={} known_email={} other_email={}",
                        form.sciper.clone(),
                        email.clone(),
                        form.email.clone()
                    ),
                    Some(form.sciper.clone()),
                )
                .await;
                return Err(YearbookApiError::Conflict.res());
            }

            db.update_user_auth_token(form.sciper.clone(), form.email.clone(), new_token.clone()).await.map_err(|err| {
                error!("post_email_login_request: unable to update user auth token: database error: {:?}", err);
                YearbookApiError::InternalError.res()
            })?;
        }
        None => {
            // new user

            db.register_unverified_user(
                form.sciper.clone(),
                form.email.clone(),
                new_token.clone(),
                official_firstname,
                official_lastname,
                official_program,
            )
            .await
            .map_err(|err| {
                error!("post_email_login_request: unable to register unverified user auth: database error: {:?}", err);
                YearbookApiError::InternalError.res()
            })?;
            db.log(
                LogType::UserNew,
                format!(
                    "Registered unverified user with email login: sciper={} email={}",
                    form.sciper.clone(),
                    form.email.clone()
                ),
                Some(form.sciper.clone()),
            )
            .await;
        }
    };

    mailing::send_login_mail(&form.email, &new_token)
        .map_err(|err| {
            match err {
                MailingError::AddressParsingError => YearbookApiError::FormatError,
                MailingError::InternalError => YearbookApiError::InternalError,
            }
            .res()
        })
        .await?;
    db.log(
        LogType::UserInfo,
        format!("Email sent with login token={}", new_token),
        Some(form.sciper.clone()),
    )
    .await;

    Ok(Json(PostEmailLoginResponse {
        warning_email: false,
        warning_list: false,
    }))
}

#[derive(ToSchema, Deserialize)]
pub struct PostEmailLoginCallback {
    auth_token: String,
}

#[derive(ToSchema, Serialize)]
pub struct PostEmailLoginCallbackResponse {
    session_key: String,
}

/// Email Login Callback
#[utoipa::path(
    request_body = PostEmailLoginCallback,
    responses(
        (status = 200, body = PostEmailLoginCallbackResponse),
        (status = 400, body = YearbookApiError),
        (status = 404, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
    security()
)]
#[post("/api/login/email-login-callback", format = "json", data = "<form>")]
pub async fn post_email_login_callback(
    form: Json<PostEmailLoginCallback>,
    db: &crate::DB,
) -> YearbookApiResponse<PostEmailLoginCallbackResponse> {
    let user = db
        .update_verified_user(form.auth_token.clone())
        .await
        .map_err(|err| {
            match err {
                DatabaseError::NotFound => YearbookApiError::NotFound,
                _ => YearbookApiError::InternalError,
            }
            .res()
        })?;

    let user_email = user
        .email
        .expect("post_email_login_callback: missing user email");
    db.log(
        LogType::UserUpdate,
        format!(
            "Account with sciper={} has been verified for email={}",
            user.sciper.clone(),
            user_email.clone()
        ),
        Some(user.sciper.clone()),
    )
    .await;

    let expected_user = db
        .get_expected_user(user.sciper.clone())
        .await
        .map_err(|err| {
            error!(
                "post_email_login_callback: unable to get expected user={}: {:?}",
                user.sciper, err
            );
            YearbookApiError::InternalError.res()
        })?;

    let account_flagged = !expected_user.mail_perso.contains(&user_email);
    if account_flagged {
        db.create_user_report(
            Some(user.sciper.clone()),
            ReportType::EmailFlagged,
            format!(
                "Email mismatch: provided email '{}' vs email in list '{}'",
                user_email, expected_user.mail_perso
            ),
        )
        .await
        .map_err(|err| {
            error!(
                "post_email_login_callback: unable to flag account user={}: database error: {:?}",
                user.sciper, err
            );
            YearbookApiError::InternalError.res()
        })?;
        db.log(
            LogType::UserWarn,
            format!("Flagged account: sciper={}", user.sciper.clone()),
            Some(user.sciper.clone()),
        )
        .await;
    }

    Ok(Json(PostEmailLoginCallbackResponse {
        session_key: user
            .session_key
            .expect("post_email_login_callback: missing session key"),
    }))
}
