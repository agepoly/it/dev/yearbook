use rocket::Route;
use utoipa::{
    openapi::security::{self, HttpAuthScheme, SecurityScheme},
    Modify, OpenApi,
};
use utoipauto::utoipauto;

mod login;
mod user;
mod admin;

#[utoipauto]
#[derive(OpenApi)]
#[openapi(
    servers(
        (url = "http://localhost:4000"),
        (url = "https://yearbook.agepoly.ch")
    ),
    components(
        schemas(user::PostUserProfilePicture, user::PostUserOtherPicture)
    )
    security(
        ("api_key" = []),
    ),
    modifiers(&ApiDocSecurityAddon)
)]
pub struct ApiDoc;

pub struct ApiDocSecurityAddon;

impl Modify for ApiDocSecurityAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        let components = openapi.components.as_mut().unwrap(); // we can unwrap safely since there already is components registered.
        components.add_security_scheme(
            "api_key",
            SecurityScheme::Http(security::Http::new(HttpAuthScheme::Bearer)),
        )
    }
}

pub fn get_routes() -> Vec<Route> {
    vec![
        routes![health_check],
        login::get_routes(),
        user::get_routes(),
        admin::get_routes()
    ]
    .concat()
}

/// Simple health check
///
/// Simple health check for infrastructure probes
#[utoipa::path(
    responses(
        (status = 200, description = "Health Ok"),
    ),
    security(),
)]
#[get("/api/health-check")]
fn health_check() -> &'static str {
    "Ok!"
}
