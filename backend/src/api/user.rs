use crate::utilities::{
    database::DatabaseError,
    img_proc::{self, FileType},
    models::{
        LogType, ProfilePictureMetaFrameSpecs, Program, ReportType, UploadStatus, UserData,
        YearbookApiError, YearbookApiResponse,
    },
    session::UserSession,
};
use log::{error, warn};
use rocket::{
    form::Form,
    fs::{NamedFile, TempFile},
    serde::json::Json,
    Route,
};
use serde::Deserialize;
use utoipa::ToSchema;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_user_data,
        post_user_identity,
        post_user_profile_picture,
        get_user_profile_picture_status,
        get_user_profile_picture,
        delete_user_profile_picture,
        post_user_profile_picture_frame,
        post_user_other_picture,
        get_user_other_picture,
        delete_user_other_picture,
        post_user_extra,
        post_user_confirm,
        post_user_report_list_error,
        post_user_report_rembg
    ]
}

/// Get all data related to the user (personal, meta, files, ...)
#[utoipa::path(
    responses(
        (status = 200, body = UserData),
        (status = 403, body = YearbookApiError),
    )
)]
#[get("/api/user/data")]
async fn get_user_data(db: &crate::DB, session: UserSession) -> YearbookApiResponse<UserData> {
    let data = db.get_user_data(session.sciper).await.map_err(|err| {
        error!("R: get_user_data: {:?}", err);
        YearbookApiError::InternalError.res()
    })?;
    Ok(Json(data))
}

#[derive(ToSchema, Deserialize, Debug)]
pub struct PostUserIdentityForm {
    preferred_firstname: String,
    preferred_lastname: String,
    preferred_program: Program,
}

/// User Identity Update
///
/// Used by users to update identity-related fields (preferred firstname, lastname and program)
#[utoipa::path(
    request_body = PostUserIdentityForm,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/user/identity", format = "json", data = "<form>")]
pub async fn post_user_identity(
    form: Json<PostUserIdentityForm>,
    db: &crate::DB,
    session: UserSession,
) -> YearbookApiResponse<()> {
    db.update_user_identity(
        session.sciper.clone(),
        form.preferred_firstname.clone(),
        form.preferred_lastname.clone(),
        Some(form.preferred_program),
    )
    .await
    .map_err(|_err| YearbookApiError::InternalError.res())?;
    db.log(
        LogType::UserUpdate,
        format!(
            "Updated user identity sciper={} : {:?}",
            session.sciper.clone(),
            &form
        ),
        Some(session.sciper.clone()),
    )
    .await;
    Ok(Json(()))
}

#[derive(ToSchema, FromForm)]
pub struct PostUserProfilePicture<'f> {
    #[schema(value_type = String, format = Binary)]
    file: TempFile<'f>,
}

/// User profile picture upload
///
/// Upload and replace user's profile picture
#[utoipa::path(
    request_body(content_type = "multipart/form-data", content = PostUserProfilePicture),
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/user/profile-picture", data = "<form>")]
pub async fn post_user_profile_picture(
    mut form: Form<PostUserProfilePicture<'_>>,
    db: &crate::DB,
    session: UserSession,
) -> YearbookApiResponse<()> {
    debug!("post_user_profile_picture: user={}", session.sciper);
    let default_ae = 10;
    let original_filename = form.file.name().unwrap_or_default().to_owned();
    let new_path = img_proc::get_file_path(FileType::ProfilePicOriginal(session.sciper.clone()));
    form.file.move_copy_to(&new_path).await.map_err(|err| {
        error!("post_user_other_picture: unable to persist file: {:?}", err);
        YearbookApiError::InternalError.res()
    })?;

    let default_meta = match img_proc::ingest_profile_picture(
        new_path,
        original_filename,
        default_ae,
        false
    )
    .await
    {
        Ok(m) => m,
        Err(err) => {
            warn!(
                "post_user_other_picture: profile picture doesn't meet requirements: {:?}",
                err
            );
            db.update_profile_picture_status(session.sciper.clone(), UploadStatus::Error)
                    .await
                    .map_err(|_| {
                        error!(
                            "post_user_other_picture: unable to update file status to error for user={} !",
                            session.sciper
                        );
                        YearbookApiError::InternalError.res()
                    })?;
            return Err(YearbookApiError::FormatError.res());
        }
    };

    db.update_profile_picture_status_and_meta(session.sciper.clone(), UploadStatus::ProcessingRembg, Some(default_meta.clone()))
        .await
        .map_err(|_| {
            error!("post_user_profile_picture: unable to update profile picture status to PROCESSING_REMBG for user={}", session.sciper);
            YearbookApiError::InternalError.res()
        })?;

    // Executed after http response : rembg + cropping
    let db = db.inner().clone();
    let sciper = session.sciper.clone();
    tokio::task::spawn(async move {
        let status = match img_proc::remove_background(sciper.clone(), default_ae).await {
            Ok(()) => {
                // Rembg is done and successful
                let last_meta = match db
                    .update_profile_picture_status_and_get_meta(
                        session.sciper.clone(),
                        UploadStatus::ProcessingFrame,
                    )
                    .await
                {
                    Ok(Some(meta)) => meta,
                    _ => {
                        error!("post_user_profile_picture: unable to update profile picture status to PROCESSING_FRAME for user={}", session.sciper);
                        return;
                    }
                };

                // Start processing default frame
                match img_proc::update_cropped_profile_picture(sciper.clone(), last_meta, false).await {
                    Ok(()) => UploadStatus::Ready,
                    Err(err) => {
                        error!(
                            "post_user_profile_picture: processing cropping error: {:?}",
                            err
                        );
                        UploadStatus::Error
                    }
                }
            }
            Err(err) => {
                error!(
                    "post_user_profile_picture: processing rembg error: {:?}",
                    err
                );
                UploadStatus::Error
            }
        };

        if let Err(err) = db.update_profile_picture_status(sciper, status).await {
            error!(
                "post_user_profile_picture: unable to update database: {:?}",
                err
            );
        }
    });

    Ok(Json(()))
}

#[derive(ToSchema, FromForm)]
pub struct PostUserOtherPicture<'f> {
    #[schema(value_type = String, format = Binary)]
    file: TempFile<'f>,
}

/// User other picture upload
///
/// Upload or replace user's other picture if same index
#[utoipa::path(
    request_body(content_type = "multipart/form-data", content = PostUserOtherPicture),
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/user/other-picture/<index>", data = "<form>")]
pub async fn post_user_other_picture(
    mut form: Form<PostUserOtherPicture<'_>>,
    db: &crate::DB,
    session: UserSession,
    index: i32,
) -> YearbookApiResponse<()> {
    debug!("post_user_other_picture: user={}", session.sciper);

    let new_path = img_proc::get_file_path(FileType::OtherOriginal(session.sciper.clone(), index));
    form.file.move_copy_to(&new_path).await.map_err(|err| {
        error!("post_user_other_picture: unable to persist file: {:?}", err);
        YearbookApiError::InternalError.res()
    })?;

    db.update_other_picture(session.sciper.clone(), index, UploadStatus::Ready)
        .await
        .map_err(|_| {
            error!("post_user_other_picture: unable to update other picture status to READY for user={}", session.sciper);
            YearbookApiError::InternalError.res()
        })?;
    db.log(
        LogType::UserUpdate,
        format!(
            "Uploaded other picture sciper={} index={}",
            session.sciper.clone(),
            index
        ),
        Some(session.sciper.clone()),
    )
    .await;

    Ok(Json(()))
}

/// Delete user other picture
#[utoipa::path(
    responses(
        (status = 200),
        (status = 500, body = YearbookApiError)
    ),
)]
#[delete("/api/user/other-picture/<index>")]
pub async fn delete_user_other_picture(
    db: &crate::DB,
    session: UserSession,
    index: i32,
) -> YearbookApiResponse<()> {
    info!("delete_user_other_picture: user={}", session.sciper);
    db.update_other_picture(session.sciper.clone(), index, UploadStatus::Empty)
    .await
    .map_err(|_| {
        error!("delete_user_other_picture: unable to update other picture i={} status to EMPTY for user={}", index, session.sciper);
        YearbookApiError::InternalError.res()
    })?;
    db.log(
        LogType::UserUpdate,
        format!(
            "Deleted other picture sciper={} index={}",
            session.sciper.clone(),
            index
        ),
        Some(session.sciper.clone()),
    )
    .await;

    img_proc::delete_other_picture(session.sciper.clone(), index)
        .await
        .map_err(|_err| {
            error!(
                "delete_user_other_picture: unable to delete other picture i={} files for user={}",
                index, session.sciper
            );
            YearbookApiError::InternalError.res()
        })?;

    Ok(Json(()))
}

/// Get other picture by index
#[utoipa::path(
    responses(
        (status = 200, content_type = "image/png"),
        (status = 404),
    )
)]
#[get("/api/user/other-picture/<index>?<key>")]
async fn get_user_other_picture(db: &crate::DB, index: i32, key: String) -> Option<NamedFile> {
    match db.session_from_key(key).await {
        Ok(Some(session)) => {
            let p = img_proc::get_file_path(FileType::OtherOriginal(session.sciper, index));
            NamedFile::open(p).await.ok()
        }
        Ok(None) => None,
        Err(err) => {
            error!("get_user_other_picture: database error: {:?}", err);
            None
        }
    }
}

/// User profile picture status
///
/// Get the status of the profile picture
#[utoipa::path(
    responses(
        (status = 200, body = UploadStatus),
        (status = 400, body = YearbookApiError),
        (status = 403, body = YearbookApiError),
    )
)]
#[get("/api/user/profile-picture-status")]
async fn get_user_profile_picture_status(
    db: &crate::DB,
    session: UserSession,
) -> YearbookApiResponse<UploadStatus> {
    let user = db.get_user_data(session.sciper).await.map_err(|err| {
        error!(
            "get_user_profile_picture_status: unable to get user data: {:?}",
            err
        );
        YearbookApiError::InternalError.res()
    })?;

    let status = user.user.profile_picture_status;
    if status == UploadStatus::Error || status == UploadStatus::Empty {
        error!(
            "get_user_profile_picture_status: returning a problematic state to the user: {:?}",
            status
        );
    }

    Ok(Json(status))
}

/// User profile picture preview
///
/// Get the processed version of the profile picture
#[utoipa::path(
    responses(
        (status = 200, content_type = "image/png"),
        (status = 404),
    )
)]
#[get("/api/user/profile-picture?<key>&<ae>&<cropped>")]
async fn get_user_profile_picture(
    db: &crate::DB,
    key: String,
    ae: Option<i32>,
    cropped: Option<bool>,
) -> Option<NamedFile> {
    match db.session_from_key(key).await {
        Ok(Some(session)) => {
            let pp_path = match (cropped, ae) {
                (Some(true), _) => {
                    if session.profile_picture_status != UploadStatus::Ready {
                        return None;
                    }
                    img_proc::get_file_path(FileType::ProfilePicFramed(session.sciper))
                }
                (_, Some(ae)) => {
                    if session.profile_picture_status != UploadStatus::Ready {
                        return None;
                    }
                    img_proc::get_file_path(FileType::ProfilePicRemBG(session.sciper, ae))
                }
                (_, None) => img_proc::get_file_path(FileType::ProfilePicOriginal(session.sciper)),
            };
            NamedFile::open(pp_path).await.ok()
        }
        Ok(None) => None,
        Err(err) => {
            error!("get_user_profile_picture: database error: {:?}", err);
            None
        }
    }
}

/// Delete user profile picture
#[utoipa::path(
    responses(
        (status = 200),
        (status = 500, body = YearbookApiError)
    ),
)]
#[delete("/api/user/profile-picture")]
pub async fn delete_user_profile_picture(
    db: &crate::DB,
    session: UserSession,
) -> YearbookApiResponse<()> {
    info!("delete_user_profile_picture: user={}", session.sciper);
    db.delete_profile_picture(session.sciper.clone())
    .await
    .map_err(|_| {
        error!("delete_user_profile_picture: unable to update profile picture status to EMPTY for user={}", session.sciper);
        YearbookApiError::InternalError.res()
    })?;
    db.log(
        LogType::UserUpdate,
        format!(
            "Deleted user profile picture sciper={}",
            session.sciper.clone(),
        ),
        Some(session.sciper.clone()),
    )
    .await;

    img_proc::delete_profile_picture(session.sciper.clone())
        .await
        .map_err(|_err| {
            error!(
                "delete_user_profile_picture: unable to delete profile picture files for user={}",
                session.sciper
            );
            YearbookApiError::InternalError.res()
        })?;

    Ok(Json(()))
}

#[derive(ToSchema, Deserialize)]
pub struct PostUserProfilePictureFrame {
    frame_specs: ProfilePictureMetaFrameSpecs,
}

/// User Profile Picture cropping
///
/// Users save the fractional dimensions of their preferred cropping
#[utoipa::path(
    request_body = PostUserProfilePictureFrame,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 409, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/user/profile-picture/frame", format = "json", data = "<form>")]
pub async fn post_user_profile_picture_frame(
    form: Json<PostUserProfilePictureFrame>,
    db: &crate::DB,
    session: UserSession,
) -> YearbookApiResponse<()> {
    let (meta, status) = db
        .update_profile_picture_frame(session.sciper.clone(), form.frame_specs.clone())
        .await
        .map_err(|err| {
            match err {
                DatabaseError::ImageFormat => YearbookApiError::FormatError,
                DatabaseError::NotFound | DatabaseError::JsonFormat => {
                    error!(
                        "post_user_profile_picture_frame: protocol error returned to client: {:?}",
                        err
                    );
                    YearbookApiError::ProtocolError
                }
                DatabaseError::Conflict => {
                    error!(
                        "post_user_profile_picture_frame: conflict happend, user={} should retry",
                        session.sciper.clone()
                    );
                    YearbookApiError::Conflict
                }
                _ => YearbookApiError::InternalError,
            }
            .res()
        })?;
    db.log(
        LogType::UserUpdate,
        format!(
            "Updated user profile picture frame sciper={} specs={:?}",
            session.sciper.clone(),
            form.frame_specs
        ),
        Some(session.sciper.clone()),
    )
    .await;

    if status == UploadStatus::ProcessingFrame {
        let db = db.inner().clone();
        let sciper = session.sciper.clone();
        tokio::task::spawn(async move {
            let status = match img_proc::update_cropped_profile_picture(sciper.clone(), meta, false).await
            {
                Ok(()) => UploadStatus::Ready,
                Err(err) => {
                    error!(
                        "post_user_profile_picture_frame: processing task error: {:?}",
                        err
                    );
                    UploadStatus::Error
                }
            };
            if let Err(err) = db.update_profile_picture_status(sciper, status).await {
                error!(
                    "post_user_profile_picture_frame: processing task error: unable to update database: {:?}",
                    err
                );
            }
        });
    }

    Ok(Json(()))
}

#[derive(ToSchema, Deserialize, Debug)]
pub struct PostUserExtraForm {
    question_teacher: Option<String>,
}

/// Extra form Update
#[utoipa::path(
    request_body = PostUserExtraForm,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/user/extra", format = "json", data = "<form>")]
pub async fn post_user_extra(
    form: Json<PostUserExtraForm>,
    db: &crate::DB,
    session: UserSession,
) -> YearbookApiResponse<()> {
    db.update_user_extra(session.sciper.clone(), form.question_teacher.clone())
        .await
        .map_err(|_err| YearbookApiError::InternalError.res())?;
    db.log(
        LogType::UserUpdate,
        format!(
            "Updated user extra questions sciper={} form={:?}",
            session.sciper.clone(),
            form
        ),
        Some(session.sciper.clone()),
    )
    .await;
    Ok(Json(()))
}

/// Final form confirmation
#[utoipa::path(
    request_body = PostUserExtraForm,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/user/confirm", format = "json")]
pub async fn post_user_confirm(db: &crate::DB, session: UserSession) -> YearbookApiResponse<()> {
    let user = db
        .get_user(session.sciper.clone())
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?
        .ok_or(YearbookApiError::NotFound.res())?;

    if !user.identity_verified {
        error!(
            "post_user_confirm: user={} identity not verified",
            session.sciper
        );
        return Err(YearbookApiError::ProtocolError.res());
    }

    if user.preferred_program.is_none()
        || user.preferred_firstname.is_none()
        || user.preferred_lastname.is_none()
    {
        error!(
            "post_user_confirm: user={} preferred identity is incomplete",
            session.sciper
        );
        db.log(
            LogType::UserError,
            format!(
                "User sciper={} final validation failed: preferred identity is incomplete",
                session.sciper.clone()
            ),
            Some(session.sciper.clone()),
        )
        .await;
        return Err(YearbookApiError::ProtocolError.res());
    }

    if user.profile_picture_status != UploadStatus::Ready {
        error!(
            "post_user_confirm: user={} profile picture not ready",
            session.sciper
        );
        db.log(
            LogType::UserError,
            format!(
                "User sciper={} final validation failed: profile picture not ready",
                session.sciper.clone()
            ),
            Some(session.sciper.clone()),
        )
        .await;
        return Err(YearbookApiError::ProtocolError.res());
    }

    db.update_user_form_validated(session.sciper.clone(), true)
        .await
        .map_err(|_err| YearbookApiError::InternalError.res())?;
    db.log(
        LogType::UserError,
        format!(
            "User sciper={} final validation succeeded",
            session.sciper.clone()
        ),
        Some(session.sciper.clone()),
    )
    .await;

    Ok(Json(()))
}

#[derive(ToSchema, Deserialize, Debug)]
pub struct PostUserReportListErrorForm {
    sciper: String,
    email: String,
    message: String,
}

/// Create list error user report
#[utoipa::path(
    request_body = PostUserReportListErrorForm,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/user/report/list-error", format = "json", data = "<form>")]
pub async fn post_user_report_list_error(
    form: Json<PostUserReportListErrorForm>,
    db: &crate::DB,
) -> YearbookApiResponse<()> {
    db.create_user_report(
        None,
        ReportType::ListError,
        format!("sciper={} email={} message={}", form.sciper, form.email, form.message),
    )
    .await
    .map_err(|_err| YearbookApiError::InternalError.res())?;
    db.log(
        LogType::UserWarn,
        format!(
            "User with sciper={} and email={} submitted a list error report: {}",
            form.sciper,
            form.email,
            form.message
        ),
        Some(form.sciper.clone()),
    )
    .await;
    Ok(Json(()))
}

#[derive(ToSchema, Deserialize, Debug)]
pub struct PostUserReportRembgForm {
    message: String,
}

/// Create rembg user report
#[utoipa::path(
    request_body = PostUserReportRembgForm,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/user/report/rembg", format = "json", data = "<form>")]
pub async fn post_user_report_rembg(
    form: Json<PostUserReportRembgForm>,
    db: &crate::DB,
    session: UserSession,
) -> YearbookApiResponse<()> {
    db.create_user_report(
        Some(session.sciper.clone()),
        ReportType::RembgReport,
        form.message.clone(),
    )
    .await
    .map_err(|_err| YearbookApiError::InternalError.res())?;
    db.log(
        LogType::UserWarn,
        format!(
            "User sciper={} submitted a rembg report: {}",
            session.sciper,
            form.message
        ),
        Some(session.sciper.clone()),
    )
    .await;
    Ok(Json(()))
}
