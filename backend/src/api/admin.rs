use std::collections::HashMap;

use crate::utilities::{
    csv,
    img_proc::{self, FileType},
    models::{
        ExpectedUser, ExpectedUserJoined, Log, ProfilePictureMetaFrameSpecs, Report, UploadStatus,
        User, YearbookApiError, YearbookApiResponse,
    },
};
use rocket::{fs::NamedFile, serde::json::Json, Route};
use serde::Deserialize;
use utoipa::ToSchema;

pub fn get_routes() -> Vec<Route> {
    routes![
        get_expected_users,
        post_expected_users,
        get_users,
        get_user,
        get_reports,
        post_report,
        get_admin_profile_picture,
        get_admin_profile_picture_thumbnail,
        post_admin_profile_picture_rembg,
        post_admin_profile_picture_frame,
        get_logs,
        post_admin_user_rejected
    ]
}

#[must_use]
fn check_admin_key(key: &str) -> YearbookApiResponse<()> {
    let env_key = &crate::config().admin_secret;
    if *key == *env_key {
        Ok(().into())
    } else {
        Err(YearbookApiError::Forbidden.res())
    }
}

/// Get the list of all users
#[utoipa::path(
    responses(
        (status = 200, body = Vec<User>),
        (status = 403, body = YearbookApiError),
    )
)]
#[get("/api/admin/users?<admin_key>")]
async fn get_users(db: &crate::DB, admin_key: String) -> YearbookApiResponse<Vec<User>> {
    check_admin_key(&admin_key)?;
    let data: Vec<User> = db
        .get_users()
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?;
    Ok(Json(data))
}

/// Get specific user
#[utoipa::path(
    responses(
        (status = 200, body = Option<User>),
        (status = 403, body = YearbookApiError),
    )
)]
#[get("/api/admin/user/<sciper>?<admin_key>")]
async fn get_user(
    db: &crate::DB,
    sciper: String,
    admin_key: String,
) -> YearbookApiResponse<Option<User>> {
    check_admin_key(&admin_key)?;
    let data: Option<User> = db
        .get_user(sciper)
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?;
    Ok(Json(data))
}

/// Get the list of all expected users
#[utoipa::path(
    responses(
        (status = 200, body = Vec<ExpectedUserJoined>),
        (status = 403, body = YearbookApiError),
    )
)]
#[get("/api/admin/expected-users?<admin_key>")]
async fn get_expected_users(
    db: &crate::DB,
    admin_key: String,
) -> YearbookApiResponse<Vec<ExpectedUserJoined>> {
    check_admin_key(&admin_key)?;
    let users_iter = db
        .get_users()
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?
        .into_iter()
        .map(|u| (u.sciper.clone(), u));
    let users: HashMap<String, User> = HashMap::from_iter(users_iter);

    let expected_users: Vec<ExpectedUser> = db
        .get_expected_users()
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?;

    let expected_users_joined: Vec<ExpectedUserJoined> = expected_users
        .into_iter()
        .map(|e| {
            let sciper = e.sciper.clone();
            e.join_clone(users.get(&sciper))
        })
        .collect();
    Ok(Json(expected_users_joined))
}

#[derive(ToSchema, Deserialize)]
pub struct PostExpectedUsers {
    admin_key: String,
    csv_string: String,
}

/// Update the list of expected users
#[utoipa::path(
    request_body = PostExpectedUsers,
    responses(
        (status = 200, body = Vec<ExpectedUser>),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/admin/expected-users", format = "json", data = "<form>")]
pub async fn post_expected_users(
    form: Json<PostExpectedUsers>,
    db: &crate::DB,
) -> YearbookApiResponse<Vec<ExpectedUser>> {
    check_admin_key(&form.admin_key)?;

    let expected_users = csv::parse_expected_users_csv(form.csv_string.clone())
        .map_err(|_| YearbookApiError::FormatError.res())?;

    db.update_expected_users(expected_users.clone())
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?;

    Ok(Json(expected_users))
}

/// Get the list of all reports
#[utoipa::path(
    responses(
        (status = 200, body = Vec<Report>),
        (status = 403, body = YearbookApiError),
    )
)]
#[get("/api/admin/reports?<sciper>&<admin_key>")]
async fn get_reports(
    db: &crate::DB,
    sciper: Option<String>,
    admin_key: String,
) -> YearbookApiResponse<Vec<Report>> {
    check_admin_key(&admin_key)?;
    let data: Vec<Report> = db
        .get_reports(sciper)
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?;
    Ok(Json(data))
}

#[derive(ToSchema, Deserialize)]
pub struct PostReport {
    admin_key: String,
    report_id: i32,
    done: bool,
}

/// Update the status of a report
#[utoipa::path(
    request_body = PostReport,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/admin/report", format = "json", data = "<form>")]
pub async fn post_report(form: Json<PostReport>, db: &crate::DB) -> YearbookApiResponse<()> {
    check_admin_key(&form.admin_key)?;

    db.update_report_done(form.report_id, form.done)
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?;

    Ok(Json(()))
}

/// Admin user profile picture preview
///
/// Get the processed version of the profile picture
#[utoipa::path(
    responses(
        (status = 200, content_type = "image/png"),
        (status = 404),
    )
)]
#[get("/api/admin/profile-picture-thumbnail/<sciper>?<key>")]
async fn get_admin_profile_picture_thumbnail(
    db: &crate::DB,
    sciper: &str,
    key: String,
) -> Option<NamedFile> {
    check_admin_key(&key).ok()?;
    match db.get_user(sciper.to_owned()).await {
        Ok(Some(user)) => {
            let thumb_path =
                img_proc::get_file_path(FileType::ProfilePicThumbnail(user.sciper.clone()));

            match NamedFile::open(thumb_path.clone()).await {
                Ok(f) => Some(f),
                Err(err) => {
                    if img_proc::create_thumbnail(FileType::ProfilePicFramed(user.sciper)).is_err()
                    {
                        error!("get_admin_profile_picture_thumbnail: unable to create thumbnail: {err:?}");
                        return None;
                    }
                    NamedFile::open(thumb_path).await.ok()
                }
            }
        }
        Ok(None) => None,
        Err(err) => {
            error!(
                "get_admin_profile_picture_thumbnail: database error: {:?}",
                err
            );
            None
        }
    }
}

/// User profile picture preview
///
/// Get the processed version of the profile picture
#[utoipa::path(
    responses(
        (status = 200, content_type = "image/png"),
        (status = 404),
    )
)]
#[get("/api/admin/profile-picture/<sciper>?<key>&<ae>&<cropped>&<admin_override>")]
async fn get_admin_profile_picture(
    db: &crate::DB,
    sciper: String,
    key: String,
    ae: Option<i32>,
    cropped: Option<bool>,
    admin_override: Option<bool>,
) -> Option<NamedFile> {
    check_admin_key(&key).ok()?;
    match db.get_user(sciper).await {
        Ok(Some(user)) => {
            let pp_path = match (admin_override, cropped, ae) {
                (Some(true), _, _) => {
                    img_proc::get_file_path(FileType::ProfilePicOverride(user.sciper))
                }
                (_, Some(true), _) => {
                    if user.profile_picture_status != UploadStatus::Ready {
                        return None;
                    }
                    img_proc::get_file_path(FileType::ProfilePicFramed(user.sciper))
                }
                (_, _, Some(ae)) => {
                    if user.profile_picture_status != UploadStatus::Ready {
                        return None;
                    }
                    img_proc::get_file_path(FileType::ProfilePicRemBG(user.sciper, ae))
                }
                (_, _, None) => img_proc::get_file_path(FileType::ProfilePicOriginal(user.sciper)),
            };
            NamedFile::open(pp_path).await.ok()
        }
        Ok(None) => None,
        Err(err) => {
            error!("get_admin_profile_picture: database error: {:?}", err);
            None
        }
    }
}

#[derive(ToSchema, Deserialize)]
pub struct PostAdminProfilePictureRemBG {
    key: String,
    ae: i32,
}

/// User profile picture upload
///
/// Upload and replace user's profile picture
#[utoipa::path(
    request_body = PostAdminProfilePictureRemBG,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/admin/profile-picture/<sciper>/rembg", data = "<form>")]
pub async fn post_admin_profile_picture_rembg(
    form: Json<PostAdminProfilePictureRemBG>,
    sciper: &str,
) -> YearbookApiResponse<()> {
    check_admin_key(&form.key)?;
    img_proc::remove_background(sciper.to_owned(), form.ae)
        .await
        .map_err(|err| {
            error!(
                "post_user_profile_picture: processing rembg error: {:?}",
                err
            );
            YearbookApiError::InternalError.res()
        })?;

    Ok(Json(()))
}

#[derive(ToSchema, Deserialize)]
pub struct PostAdminProfilePictureFrame {
    key: String,
    ae: i32,
    frame_specs: ProfilePictureMetaFrameSpecs,
}

/// User Profile Picture cropping
#[utoipa::path(
    request_body = PostAdminProfilePictureFrame,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 409, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post(
    "/api/admin/profile-picture/<sciper>/frame-override",
    format = "json",
    data = "<form>"
)]
pub async fn post_admin_profile_picture_frame(
    form: Json<PostAdminProfilePictureFrame>,
    db: &crate::DB,
    sciper: &str,
) -> YearbookApiResponse<()> {
    check_admin_key(&form.key)?;
    match db
        .get_user(sciper.to_owned())
        .await
        .map_err(|_| YearbookApiError::ProtocolError.res())?
    {
        Some(user) => {
            let mut meta = user
                .profile_picture_meta
                .ok_or_else(|| YearbookApiError::FormatError.res())?;
            meta.ae = form.ae;
            meta.frame_specs = form.frame_specs.clone();
            img_proc::update_cropped_profile_picture(sciper.to_owned(), meta, true)
                .await
                .map_err(|err| {
                    error!(
                        "post_user_profile_picture_frame: update_cropped_profile_picture: {err:?}"
                    );
                    YearbookApiError::InternalError.res()
                })?;

            Ok(Json(()))
        }
        None => Err(YearbookApiError::NotFound.res()),
    }
}

/// Get the list of logs
#[utoipa::path(
    responses(
        (status = 200, body = Vec<Log>),
        (status = 403, body = YearbookApiError),
    )
)]
#[get("/api/admin/logs?<admin_key>&<user>")]
async fn get_logs(
    db: &crate::DB,
    admin_key: String,
    user: Option<String>,
) -> YearbookApiResponse<Vec<Log>> {
    check_admin_key(&admin_key)?;
    let data: Vec<Log> = db
        .get_logs(user)
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?;
    Ok(Json(data))
}

#[derive(ToSchema, Deserialize)]
pub struct PostAdminUserRejected {
    admin_key: String,
    rejected: bool,
}

/// Update the rejection status of a user
#[utoipa::path(
    request_body = PostAdminUserRejected,
    responses(
        (status = 200),
        (status = 400, body = YearbookApiError),
        (status = 500, body = YearbookApiError)
    ),
)]
#[post("/api/admin/user/<sciper>/rejected", format = "json", data = "<form>")]
pub async fn post_admin_user_rejected(form: Json<PostAdminUserRejected>, sciper: &str, db: &crate::DB) -> YearbookApiResponse<()> {
    check_admin_key(&form.admin_key)?;

    db.update_user_rejected(sciper.to_owned(), form.rejected)
        .await
        .map_err(|_| YearbookApiError::InternalError.res())?;

    Ok(Json(()))
}

// #[derive(ToSchema, Deserialize)]
// pub struct PostAdminRegenMetaForm {
//     key: String,
//     sciper: String,
//     ae: i32,
// }

// /// Regenerate user profile picture default metadata
// #[utoipa::path(
//     request_body = PostAdminRegenMetaForm,
//     responses(
//         (status = 200),
//         (status = 400, body = YearbookApiError),
//         (status = 500, body = YearbookApiError)
//     ),
// )]
// #[post("/api/admin/profile-picture-regen-meta", data = "<form>")]
// pub async fn post_admin_profile_picture_regen_meta(
//     form: Json<PostAdminRegenMetaForm>,
//     db: &crate::DB,
// ) -> YearbookApiResponse<()> {
//     check_admin_key(&form.key)?;
//     info!("post_admin_profile_picture_regen_meta: regenerating profile picture metadata for sciper={}", form.sciper);

//     let original_filename = "".to_owned();
//     let new_path = img_proc::get_file_path(FileType::ProfilePicOriginal(form.sciper.clone()));

//     let default_meta = match img_proc::ingest_profile_picture(
//         new_path,
//         original_filename,
//         form.ae,
//         true,
//     )
//     .await
//     {
//         Ok(m) => m,
//         Err(err) => {
//             warn!(
//                 "post_admin_profile_picture_regen_meta: profile picture doesn't meet requirements: {:?}",
//                 err
//             );
//             db.update_profile_picture_status(form.sciper.clone(), UploadStatus::Error)
//                     .await
//                     .map_err(|_| {
//                         error!(
//                             "post_admin_profile_picture_regen_meta: unable to update file status to error for user={} !",
//                             form.sciper
//                         );
//                         YearbookApiError::InternalError.res()
//                     })?;
//             return Err(YearbookApiError::FormatError.res());
//         }
//     };

//     img_proc::remove_background(form.sciper.clone(), form.ae).await.map_err(|err| {
//         error!(
//             "post_admin_profile_picture_regen_meta: processing rembg error: {:?}",
//             err
//         );
//         YearbookApiError::InternalError.res()
//     });

//     // Start processing default frame
//     img_proc::update_cropped_profile_picture(form.sciper.clone(), default_meta, false).await.map_err(|err| {
//         error!(
//             "post_admin_profile_picture_regen_meta: processing cropping error: {:?}",
//             err
//         );
//         YearbookApiError::InternalError.res()
//     })?;

//     if let Err(err) = db.update_profile_picture_status(sciper, status).await {
//         error!(
//             "post_admin_profile_picture_regen_meta: unable to update database: {:?}",
//             err
//         );
//     }

//     Ok(Json(()))
// }
