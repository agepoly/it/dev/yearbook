use rocket::{
    http::Status,
    outcome::try_outcome,
    request::{FromRequest, Outcome},
    Request,
};
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

use crate::DB;

use super::models::{UploadStatus, YearbookApiError};

#[derive(Serialize, Deserialize, ToSchema, Clone, Debug)]
pub struct UserSession {
    pub sciper: String,
    pub firstname: String,
    pub lastname: String,
    pub session_key: String,
    pub profile_picture_status: UploadStatus,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for UserSession {
    type Error = ();
    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let db = try_outcome!(request.guard::<&DB>().await);
        match parse_session_from_req(db, request).await {
            Ok(session) => Outcome::Success(session),
            _ => Outcome::Error((Status::Forbidden, ())),
        }
    }
}

async fn parse_session_from_req(
    db: &DB,
    request: &Request<'_>,
) -> Result<UserSession, YearbookApiError> {
    let session_key = request
        .headers()
        .get_one("authorization")
        .ok_or(YearbookApiError::Forbidden)?
        .replace("Bearer ", "");

    match db.session_from_key(session_key).await {
        Ok(Some(session)) => Ok(session),
        Ok(None) => Err(YearbookApiError::Forbidden),
        Err(_) => Err(YearbookApiError::InternalError),
    }
}