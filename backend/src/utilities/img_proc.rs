use std::path::{Path, PathBuf};

use image::GenericImageView;
use rocket::futures::TryFutureExt;

use crate::utilities::models::ProfilePictureMetaFrameSpecs;

use super::models::ProfilePictureMeta;

#[derive(Debug)]
pub enum ImageProcessingError {
    InternalError,
    InvalidSize,
    DecodingError,
}

const MIN_IMAGE_WIDTH: u32 = 510;
const MIN_IMAGE_HEIGHT: u32 = 600;
const FRAME_EXACT_RATIO: f32 = 510.0 / 600.0; // = 0.85
const FRAME_RATIO_TOLERATION: f32 = 0.01;

#[derive(Debug, Clone)]
pub enum FileType {
    /// user sciper
    ProfilePicOriginal(String),
    /// user sciper, ae value
    ProfilePicRemBG(String, i32),
    /// user sciper
    ProfilePicFramed(String),
    /// user sciper
    ProfilePicThumbnail(String),
    /// user sciper
    ProfilePicOverride(String),
    /// user sciper, index
    OtherOriginal(String, i32),
    /// user sciper, index
    OtherThumbnail(String, i32),
}

pub fn get_file_path(file_type: FileType) -> Box<Path> {
    let mut path_buf = PathBuf::from(&crate::config().files_path);
    std::fs::create_dir_all(&path_buf).expect("unable to ensure files path existance");

    let file_name = match file_type {
        FileType::ProfilePicOriginal(user) => format!("{}-profile", user),
        FileType::ProfilePicRemBG(user, ae) => format!("{}-profile-rembg-ae{}.png", user, ae),
        FileType::ProfilePicFramed(user) => format!("{}-profile-cropped.png", user),
        FileType::ProfilePicThumbnail(user) => format!("{}-profile-thumb.png", user),
        FileType::ProfilePicOverride(user) => format!("{}-profile-override.png", user),
        FileType::OtherOriginal(user, index) => format!("{}-other-{}", user, index),
        FileType::OtherThumbnail(user, index) => format!("{}-other-{}.png", user, index),
    };
    path_buf.push(file_name);
    path_buf.into_boxed_path()
}

pub fn create_thumbnail(file_type: FileType) -> Result<(), ImageProcessingError> {
    debug!(
        "create_thumbnail: generating thumbnail for {:?}...",
        file_type
    );
    let image = image::open(get_file_path(file_type.clone())).map_err(|err| {
        error!("create_thumbnail: image open error: {:?}", err);
        ImageProcessingError::InternalError
    })?;
    let new_type = match file_type {
        FileType::ProfilePicFramed(s) => Ok(FileType::ProfilePicThumbnail(s)),
        FileType::OtherOriginal(s, i) => Ok(FileType::OtherThumbnail(s, i)),
        _ => Err(ImageProcessingError::InternalError),
    }?;
    let thumb_path = get_file_path(new_type);
    image
        .thumbnail_exact(MIN_IMAGE_WIDTH, MIN_IMAGE_HEIGHT)
        .save(thumb_path)
        .map_err(|err| {
            error!("create_thumbnail: image save error: {:?}", err);
            ImageProcessingError::InternalError
        })?;
    Ok(())
}

pub fn compute_default_frame_specs(width: i32, height: i32) -> ProfilePictureMetaFrameSpecs {
    let image_ratio = (width as f32) / (height as f32);
    let s = if image_ratio < FRAME_EXACT_RATIO {
        // tall image i.e. picture width = frame width
        let frame_height = (width as f32) / FRAME_EXACT_RATIO;
        let ratio_h = frame_height / (height as f32);
        let space_y = ((height as f32) - frame_height) / 2.0;
        let ratio_y = space_y / (height as f32);
        ProfilePictureMetaFrameSpecs {
            ratio_x: 0.0,
            ratio_y,
            ratio_w: 1.0,
            ratio_h,
            zoom: 100,
        }
    } else {
        // wide image i.e. picture height = frame height
        let frame_width = (height as f32) * FRAME_EXACT_RATIO;
        let ratio_w = frame_width / (width as f32);
        let space_x = ((width as f32) - frame_width) / 2.0;
        let ratio_x = space_x / (width as f32);
        ProfilePictureMetaFrameSpecs {
            ratio_x,
            ratio_y: 0.0,
            ratio_w,
            ratio_h: 1.0,
            zoom: 100,
        }
    };
    debug!(
        "compute_default_frame_specs: width={},height={} {:?}",
        width, height, s
    );
    s
}

pub async fn ingest_profile_picture(
    img_path: Box<Path>,
    filename: String,
    ae: i32,
    ignore_dimensions_check: bool,
) -> Result<ProfilePictureMeta, ImageProcessingError> {
    debug!("ingesting profile picture path={:?}", img_path);
    let img_reader = image::io::Reader::open(img_path)
        .map_err(|err| {
            warn!("unable to open image: {:?}", err);
            ImageProcessingError::InternalError
        })?
        .with_guessed_format()
        .map_err(|err| {
            warn!("unable to guess image format: {:?}", err);
            ImageProcessingError::DecodingError
        })?;
    debug!("verify_profile_picture: decoding begins");
    let img = img_reader.decode().map_err(|err| {
        warn!("unable to decode image: {:?}", err);
        ImageProcessingError::DecodingError
    })?;

    let (width, height) = img.dimensions();
    if !ignore_dimensions_check {
        if width < MIN_IMAGE_WIDTH || height < MIN_IMAGE_HEIGHT {
            return Err(ImageProcessingError::InvalidSize);
        }
    }

    let filesize = 0;
    let (width, height) = (width as i32, height as i32);
    let frame_specs = compute_default_frame_specs(width, height);

    Ok(ProfilePictureMeta {
        ae,
        filename,
        filesize,
        width,
        height,
        frame_specs,
    })
}

pub async fn remove_background(user: String, ae: i32) -> Result<(), ImageProcessingError> {
    let input_path = get_file_path(FileType::ProfilePicOriginal(user.clone()));
    let input_arg = format!(
        "file=@{}",
        input_path
            .to_str()
            .ok_or(ImageProcessingError::InternalError)?
    );
    let output_path = get_file_path(FileType::ProfilePicRemBG(user, ae));
    let output_path = output_path
        .to_str()
        .ok_or(ImageProcessingError::InternalError)?;
    let ae = format!("ae={}", ae);
    let url = format!("{}/api/remove", crate::config().bgrem_base_url);
    let status = tokio::process::Command::new("curl")
        .args([
            "-F",
            &input_arg,
            "-F",
            "a=true",
            "-F",
            &ae,
            &url,
            "-o",
            &output_path,
        ])
        .status()
        .await
        .map_err(|err| {
            error!("remove_background: command error: {:?}", err);
            ImageProcessingError::InternalError
        })?;
    if status.success() {
        Ok(())
    } else {
        Err(ImageProcessingError::InternalError)
    }
}

pub fn verify_frame_meta(meta: &ProfilePictureMeta) -> bool {
    let check_frac = |v: f32| v >= 0.0 && v <= 1.0;
    if !check_frac(meta.frame_specs.ratio_x)
        || !check_frac(meta.frame_specs.ratio_y)
        || !check_frac(meta.frame_specs.ratio_w)
        || !check_frac(meta.frame_specs.ratio_h)
    {
        error!("verify_frame_specs: check_frac failed meta={:?}", meta);
        return false;
    }

    let frame_dim = meta.frame_specs.dimensions(meta.width, meta.height);
    let ratio = (frame_dim.w as f32) / (frame_dim.h as f32);
    let ratio_delta = (ratio - FRAME_EXACT_RATIO).abs();
    let ratio_in_range = ratio_delta < FRAME_RATIO_TOLERATION;
    if !ratio_in_range {
        error!(
            "verify_frame_specs: ratio={} not in range={}±{}, delta={}",
            ratio, FRAME_EXACT_RATIO, FRAME_RATIO_TOLERATION, ratio_delta
        );
        return false;
    }

    return true;
}

pub async fn update_cropped_profile_picture(
    user: String,
    meta: ProfilePictureMeta,
    admin_override: bool,
) -> Result<(), ImageProcessingError> {
    let input_path = get_file_path(FileType::ProfilePicRemBG(user.clone(), meta.ae));
    let image = image::open(input_path).map_err(|err| {
        error!(
            "update_cropped_profile_picture: image open error: {:?}",
            err
        );
        ImageProcessingError::InternalError
    })?;
    let frame_rect = meta.frame_specs.dimensions(meta.width, meta.height);
    debug!(
        "update_cropped_profile_picture: cropping profile picture: user={} w={},h={} specs={:?} frame_rect={:?}",
        user, meta.width, meta.height, meta.frame_specs, frame_rect
    );
    let cropped = image.crop_imm(
        frame_rect.x as u32,
        frame_rect.y as u32,
        frame_rect.w as u32,
        frame_rect.h as u32,
    );
    let output_file_type = match admin_override {
        true => FileType::ProfilePicOverride(user),
        false => FileType::ProfilePicFramed(user),
    };
    cropped
        .save_with_format(get_file_path(output_file_type), image::ImageFormat::Png)
        .map_err(|err| {
            error!(
                "update_cropped_profile_picture: error during save: {:?}",
                err
            );
            ImageProcessingError::InternalError
        })?;

    Ok(())
}

pub async fn delete_profile_picture(user: String) -> Result<(), ImageProcessingError> {
    let mut files = tokio::fs::read_dir(&crate::config().files_path)
        .map_err(|err| {
            error!(
                "delete_profile_picture: unable to read files directory: {:?}",
                err
            );
            ImageProcessingError::InternalError
        })
        .await?;

    while let Some(entry) = files.next_entry().await.map_err(|err| {
        error!(
            "delete_profile_picture: unable to get next entry: {:?}",
            err
        );
        ImageProcessingError::InternalError
    })? {
        let file_name = entry.file_name().into_string().map_err(|s| {
            error!(
                "delete_profile_picture: unable to convert OsString to String: {:?}",
                s
            );
            ImageProcessingError::InternalError
        })?;
        if !file_name.starts_with(&format!("{}-profile", user)) {
            continue;
        }
        info!("delete_profile_picture: deleting files/{}", file_name);
        if let Err(err) = tokio::fs::remove_file(entry.path()).await {
            error!(
                "delete_profile_picture: unable to delete {}: {:?}",
                file_name, err
            );
            return Err(ImageProcessingError::InternalError);
        }
    }

    Ok(())
}

pub async fn delete_other_picture(user: String, index: i32) -> Result<(), ImageProcessingError> {
    let mut files = tokio::fs::read_dir(&crate::config().files_path)
        .map_err(|err| {
            error!(
                "delete_other_picture: unable to read files directory: {:?}",
                err
            );
            ImageProcessingError::InternalError
        })
        .await?;

    while let Some(entry) = files.next_entry().await.map_err(|err| {
        error!("delete_other_picture: unable to get next entry: {:?}", err);
        ImageProcessingError::InternalError
    })? {
        let file_name = entry.file_name().into_string().map_err(|s| {
            error!(
                "delete_other_picture: unable to convert OsString to String: {:?}",
                s
            );
            ImageProcessingError::InternalError
        })?;
        if !file_name.starts_with(&format!("{}-other-{}", user, index)) {
            continue;
        }
        info!("delete_other_picture: deleting files/{}", file_name);
        if let Err(err) = tokio::fs::remove_file(entry.path()).await {
            error!(
                "delete_other_picture: unable to delete {}: {:?}",
                file_name, err
            );
            return Err(ImageProcessingError::InternalError);
        }
    }

    Ok(())
}
