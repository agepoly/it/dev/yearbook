use envconfig::Envconfig;
use std::sync::OnceLock;

#[derive(Envconfig)]
pub struct Config {
    #[envconfig(from = "ADMIN_SECRET")]
    pub admin_secret: String,
    #[allow(unused)] // used by prisma client, enforced here
    #[envconfig(from = "DATABASE_URL")]
    pub database_url: String,
    
    #[envconfig(from = "TEQUILA_CALLBACK_URL")]
    pub tequila_callback_url: Option<String>,

    #[envconfig(from = "FILES_PATH")]
    pub files_path: String,
    #[envconfig(from = "BGREM_BASE_URL")]
    pub bgrem_base_url: String,

    #[envconfig(from = "SMTP_FROM")]
    pub smtp_from: String,
    #[envconfig(from = "SMTP_RELAY")]
    pub smtp_relay: String,
    #[envconfig(from = "SMTP_USER")]
    pub smtp_user: String,
    #[envconfig(from = "SMTP_PASS")]
    pub smtp_pass: String,

    #[envconfig(from = "FORM_OPEN")]
    pub form_open: bool,
}

/// Uses a [`OnceLock`] such that the config is only parsed once.
static CONFIG: OnceLock<Config> = OnceLock::new();

/// Retrieves the config through environment variables. Called on startup to check that the config is valid.
/// All accesses to environment variables must be done through this API.
pub fn config() -> &'static Config {
    CONFIG.get_or_init(|| {
        if let Ok(path) = dotenv::dotenv() {
            println!("Loaded .env file (at {:?})", path);
        }

        Envconfig::init_from_env().unwrap()
    })
}
