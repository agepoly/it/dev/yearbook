use reqwest::{Client, StatusCode};
use serde::{Deserialize, Serialize};
use std::time::Duration;

const DEFAULT_HOST: &str = "https://tequila.epfl.ch/cgi-bin/tequila";
pub const CLIENT_AUTH_URL: &str = "https://tequila.epfl.ch/cgi-bin/tequila/auth?requestkey=";
const SERVICE_NAME: &str = "Yearbook - AGEPoly";
pub const REQUEST_ATTRIBUTES: &str =
    "uniqueid,email,username,name,firstname,displayname,unitresp,group,allunits,unit,where,wheres,statut,authstrength,camiprocardid";
// const REQUIRE_ATTRIBUTES: &str = "authstrength=2";
const REQUEST_TIMEOUT_S: u64 = 8;

const LINE_DELIMITER: char = '\n';
const KEY_VALUE_DELIMITER: char = '=';
const VALUE_DELIMITER: char = ',';

/// The Tequila interface, exposing calls that can be used
/// to query the remote Tequila service.
#[derive(Clone, Debug)]
pub struct Tequila {
    client: Client,
    host: String,
}

impl Tequila {
    pub fn new() -> Self {
        Self {
            client: Client::new(),
            host: DEFAULT_HOST.to_owned(),
        }
    }

    #[allow(dead_code)]
    pub fn new_with_host(host: &str) -> Self {
        Self {
            client: Client::new(),
            host: host.to_owned(),
        }
    }

    /// Carries out.rs a `createrequest` call, requesting a new sign-in
    /// flow that the user can be redirected to from Tequila.
    pub async fn create_request(&self, return_url: &str) -> Result<String, TequilaError> {
        self.make_tequila_call(
            TequilaMethod::CreateRequest,
            &[
                ("urlaccess", return_url),
                ("service", SERVICE_NAME),
                ("request", REQUEST_ATTRIBUTES),
                // ("require", REQUIRE_ATTRIBUTES),
                ("mode_auth_check", "1"),
            ],
        )
        .await
        .and_then(|response| {
            response
                .into_iter()
                // Extract the `key` line from the body
                .find(|(k, _)| k.as_str() == "key")
                .map(|(_, v)| v)
                // Assert that it's a singular value, not an array
                .and_then(|v| match v {
                    TequilaValue::Single(key) => Some(key),
                    _ => None,
                })
                // Convert Option<T> to Result<T, E>
                .ok_or_else(|| {
                    log::error!("tequila: no key in tequila response");
                    TequilaError::ProtocolError
                })
        })
    }

    /// Executes a `fetchattributes` Tequila call, returning the user's
    /// attributes after a successful authentication flow.
    pub async fn fetch_attributes(
        &self,
        key: &str,
        auth_check: &str,
    ) -> Result<Vec<(String, TequilaValue)>, TequilaError> {
        self.make_tequila_call(
            TequilaMethod::FetchAttributes,
            &[("key", key), ("auth_check", auth_check)],
        )
        .await
    }

    /// Executes a given Tequila call, returning the response.
    async fn make_tequila_call(
        &self,
        method: TequilaMethod,
        params: &[(&str, &str)],
    ) -> Result<Vec<(String, TequilaValue)>, TequilaError> {
        let url = format!("{}/{}", self.host, method.as_str());
        let response = self
            .client
            .post(&url)
            .header("Content-Type", "text/plain")
            .body(serialize_params(params))
            .timeout(Duration::from_secs(REQUEST_TIMEOUT_S))
            .send()
            .await
            .map_err(|error| {
                log::error!("tequila: connection refused: {:?}", error);
                TequilaError::ConnectionRefused
            })?;

        match response.status() {
            StatusCode::OK => {
                let response_text = response.text().await.map_err(|error| {
                    log::error!("tequila: empty response from tequila: {:?}", error);
                    TequilaError::ProtocolError
                })?;

                Ok(deserialize_params(&response_text))
            }
            StatusCode::UNAVAILABLE_FOR_LEGAL_REASONS => Err(TequilaError::BadSessionKey),

            StatusCode::FORBIDDEN => {
                warn!("Tequila refused connection, VPN may not be active!");
                Err(TequilaError::ActionForbidden)
            }

            status => {
                log::error!("tequila: unknown status code from tequila : {}", status);
                Err(TequilaError::ProtocolError)
            }
        }
    }
}

/// Serializes a list of key-value pairs delimited by equals signs and
/// new lines, as understood by Tequila.
fn serialize_params(params: &[(&str, &str)]) -> String {
    let mut serialised =
        params
            .iter()
            .flat_map(|(k, v)| [k, "=", v, "\n"])
            .fold(String::new(), |mut acc, item| {
                acc.push_str(item);
                acc
            });

    serialised.pop();
    serialised
}

/// Deserializes Tequila's equals and new line delimited payload into
/// a collection of key-value pairs.
fn deserialize_params(params: &str) -> Vec<(String, TequilaValue)> {
    params
        .trim()
        .split(LINE_DELIMITER)
        .filter_map(|line| line.split_once(KEY_VALUE_DELIMITER))
        .map(deserialize_param_value)
        .collect()
}

fn deserialize_param_value((key, value): (&str, &str)) -> (String, TequilaValue) {
    use TequilaValue::*;
    let key = key.to_string();

    let value = match value.contains(VALUE_DELIMITER) {
        true => Multiple(value.split(VALUE_DELIMITER).map(String::from).collect()),
        false => Single(value.to_string()),
    };

    (key, value)
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TequilaAttributes {
    /// The SCIPER is a String so it can handle special SCIPERs like service accounts
    pub sciper: String,
    /// Personal EPFL email address
    pub email: String,
    pub gaspar_username: String,
    pub name: String,
    pub firstname: String,
    pub display_name: String,
    /// Units where the user is registered as the Head of Unit
    pub head_of_units: Vec<String>,
    /// groups as of groups.epfl.ch
    pub groups: Vec<String>,
    /// units as of units.epfl.ch
    pub units: Vec<String>,
    pub main_unit: String,
    pub main_unit_path: String,
    pub unit_paths: Vec<String>,
    // epfl_status: String, // Will be an enum for reliable checks
    // auth_strength: u8, // Will be an enum for reliable checks
    pub camipro_card_id: String,
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum TequilaAttributeParserError {
    MissingField(Vec<String>),
    BadFormat(Option<String>),
}

impl TryFrom<Vec<(String, TequilaValue)>> for TequilaAttributes {
    type Error = TequilaAttributeParserError;
    fn try_from(vec: Vec<(String, TequilaValue)>) -> Result<Self, Self::Error> {
        let mut missing_attributes: std::collections::BTreeSet<&str> =
            super::tequila::REQUEST_ATTRIBUTES.split(',').collect();
        // Ok, it's going to get ugly. Please look away
        let mut r = TequilaAttributes {
            sciper: "".to_string(),
            email: "".to_string(),
            gaspar_username: "".to_string(),
            name: "".to_string(),
            firstname: "".to_string(),
            display_name: "".to_string(),
            head_of_units: vec![],
            groups: vec![],
            units: vec![],
            main_unit: "".to_string(),
            main_unit_path: "".to_string(),
            unit_paths: vec![],
            camipro_card_id: "".to_string(),
        };
        for (key, value) in vec {
            match key.as_str() {
                "uniqueid" => {
                    r.sciper = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "email" => {
                    r.email = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "username" => {
                    r.gaspar_username = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "name" => {
                    r.name = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "firstname" => {
                    r.firstname = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "displayname" => {
                    r.display_name = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "unitresp" => {
                    r.head_of_units = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "group" => {
                    r.groups = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "unit" => {
                    // Special case where we convert a Tequila "Multiple Value" into a simple String
                    let unit_value: Vec<String> = value.try_into()?;
                    r.main_unit = unit_value
                        .first()
                        .ok_or(Self::Error::BadFormat(Some("unit".to_owned())))?
                        .into();
                    missing_attributes.remove(key.as_str());
                }
                "where" => {
                    r.main_unit_path = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "allunits" => {
                    r.units = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "wheres" => {
                    r.unit_paths = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                "statut" => {
                    missing_attributes.remove(key.as_str());
                } // TODO
                "authstrength" => {
                    missing_attributes.remove(key.as_str());
                } // TODO
                "camiprocardid" => {
                    r.camipro_card_id = value.try_into()?;
                    missing_attributes.remove(key.as_str());
                }
                _ => {}
            }
        }
        if missing_attributes.is_empty() {
            Ok(r)
        } else {
            error!("Missing tequila attributes : {:?}", missing_attributes);
            Err(TequilaAttributeParserError::MissingField(
                missing_attributes
                    .into_iter()
                    .map(&str::to_string)
                    .collect(),
            ))
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum TequilaValue {
    Single(String),
    Multiple(Vec<String>),
}

impl TryInto<String> for TequilaValue {
    type Error = TequilaAttributeParserError;
    fn try_into(self) -> Result<String, Self::Error> {
        match self {
            Self::Single(str) => Ok(str),
            Self::Multiple(_) => Err(Self::Error::BadFormat(None)),
        }
    }
}

impl TryInto<Vec<String>> for TequilaValue {
    type Error = TequilaAttributeParserError;
    fn try_into(self) -> Result<Vec<String>, Self::Error> {
        match self {
            Self::Multiple(vec) => Ok(vec),
            Self::Single(str) => Ok(vec![str]),
        }
    }
}

#[derive(Clone, Debug)]
pub enum TequilaError {
    ActionForbidden,
    ConnectionRefused,
    ProtocolError,
    BadSessionKey,
}

#[derive(Clone, Debug)]
pub enum TequilaMethod {
    CreateRequest,
    FetchAttributes,
}

impl TequilaMethod {
    pub fn as_str(&self) -> &'static str {
        match self {
            TequilaMethod::CreateRequest => "createrequest",
            TequilaMethod::FetchAttributes => "fetchattributes",
        }
    }
}
