use log::error;
use serde::Deserialize;

use super::models::{ExpectedUser, Program};

#[derive(Debug, Deserialize, Eq, PartialEq)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ExpectedUserRawSection {
    Ar,
    Cgc,
    Dh,
    El,
    Gc,
    Gm,
    If,
    In,
    Ma,
    Mt,
    Mte,
    Mx,
    Nx,
    Ph,
    Sc,
    Sie,
    Siq,
    Sv,
}

#[derive(Debug, Deserialize, Eq, PartialEq)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ExpectedUserRawOrientation {
    CgcChim,
    CgcIng,
    ElEner,
    Mnis,
    InCs,
    IngMath,
    MaCo,
    MaStat,
    Math,
    MtRo,
    MteSmt,
    Mtee,
    Nx,
    IngPhys,
    PhNe,
    Phys,
    ScDs,
    ScEpfl,
}

#[derive(Debug, Deserialize, Eq, PartialEq)]
pub struct ExpectedUserRaw {
    pub index: i32,
    pub lastname: String,
    pub firstname: String,
    pub section: ExpectedUserRawSection,
    pub orientation: Option<ExpectedUserRawOrientation>,
    pub sciper: String,
    pub sex: String,
    pub pending: String,
    pub mail_epfl: String,
    pub mail_perso: String,
}

fn orientation_to_program(
    section: ExpectedUserRawSection,
    orientation: Option<ExpectedUserRawOrientation>,
) -> Result<Program, (ExpectedUserRawSection, Option<ExpectedUserRawOrientation>)> {
    let p = match (section, orientation) {
        (ExpectedUserRawSection::Ar, None) => Program::Ar,
        (ExpectedUserRawSection::Cgc, Some(ExpectedUserRawOrientation::CgcChim)) => Program::CgcCh,
        (ExpectedUserRawSection::Cgc, Some(ExpectedUserRawOrientation::CgcIng)) => Program::CgcMb,
        (ExpectedUserRawSection::Dh, None) => Program::Dh,
        (ExpectedUserRawSection::El, None) => Program::El,
        (ExpectedUserRawSection::El, Some(ExpectedUserRawOrientation::ElEner)) => Program::Energ,
        (ExpectedUserRawSection::El, Some(ExpectedUserRawOrientation::Mnis)) => Program::Mnis,
        (ExpectedUserRawSection::Gc, None) => Program::Gc,
        (ExpectedUserRawSection::Gm, None) => Program::Gm,
        (ExpectedUserRawSection::If, None) => Program::If,
        (ExpectedUserRawSection::In, None) => Program::Cs,
        (ExpectedUserRawSection::In, Some(ExpectedUserRawOrientation::InCs)) => Program::Cyber,
        (ExpectedUserRawSection::Ma, Some(ExpectedUserRawOrientation::IngMath)) => Program::MaApp,
        (ExpectedUserRawSection::Ma, Some(ExpectedUserRawOrientation::MaCo)) => Program::MaComp,
        (ExpectedUserRawSection::Ma, Some(ExpectedUserRawOrientation::MaStat)) => Program::Stat,
        (ExpectedUserRawSection::Ma, Some(ExpectedUserRawOrientation::Math)) => Program::Ma,
        (ExpectedUserRawSection::Mt, None) => Program::Mt,
        (ExpectedUserRawSection::Mt, Some(ExpectedUserRawOrientation::MtRo)) => Program::Robot,
        (ExpectedUserRawSection::Mte, Some(ExpectedUserRawOrientation::MteSmt)) => Program::Smt,
        (ExpectedUserRawSection::Mte, Some(ExpectedUserRawOrientation::Mtee)) => Program::Mte,
        (ExpectedUserRawSection::Mx, None) => Program::Mx,
        (ExpectedUserRawSection::Nx, Some(ExpectedUserRawOrientation::Nx)) => Program::Nx,
        (ExpectedUserRawSection::Ph, Some(ExpectedUserRawOrientation::IngPhys)) => Program::PhApp,
        (ExpectedUserRawSection::Ph, Some(ExpectedUserRawOrientation::PhNe)) => Program::Ne,
        (ExpectedUserRawSection::Ph, Some(ExpectedUserRawOrientation::Phys)) => Program::Ph,
        (ExpectedUserRawSection::Sc, Some(ExpectedUserRawOrientation::ScDs)) => Program::Ds,
        (ExpectedUserRawSection::Sc, Some(ExpectedUserRawOrientation::ScEpfl)) => Program::Sc,
        (ExpectedUserRawSection::Sie, None) => Program::Sie,
        (ExpectedUserRawSection::Siq, None) => Program::Siq,
        (ExpectedUserRawSection::Sv, None) => Program::Sv,
        (s,o) => return Err((s,o))
    };
    Ok(p)
}

pub fn parse_expected_users_csv(csv: String) -> Result<Vec<ExpectedUser>, ()> {
    let mut reader = csv::Reader::from_reader(csv.as_bytes());
    let mut iter = reader.deserialize();

    let mut expected_users: Vec<ExpectedUser> = vec![];

    while let Some(result) = iter.next() {
        let user_raw: ExpectedUserRaw = result.map_err(|err| {
            error!("parse_expected_users_csv: parsing error: {:?}", err);
        })?;
        let program = orientation_to_program(user_raw.section, user_raw.orientation).map_err(|(s,o)| {
            error!("parse_expected_users_csv: unknown master program ({:?},{:?})", s, o);
        })?;
        let mail_epfl = if user_raw.mail_epfl.len() == 0 { None } else { Some(user_raw.mail_epfl) };
        expected_users.push(ExpectedUser {
            sciper: user_raw.sciper,
            firstname: user_raw.firstname,
            lastname: user_raw.lastname,
            program,
            mail_epfl,
            mail_perso: user_raw.mail_perso,
            note: None,
        })
    }

    Ok(expected_users)
}
