use rocket::fs::NamedFile;
use rocket::request::Request;
use rocket::response::{self, Responder, Response};

pub struct CachedFile(NamedFile);

impl From<NamedFile> for CachedFile {
    fn from(f: NamedFile) -> Self {
        Self(f)
    }
}

impl<'a> Responder<'a, 'a> for CachedFile {
    fn respond_to(self, req: &Request) -> response::Result<'a> {
        Response::build_from(self.0.respond_to(req)?)
            .raw_header("Cache-control", "max-age=86400") //  24h (24*60*60)
            .ok()
    }
}
