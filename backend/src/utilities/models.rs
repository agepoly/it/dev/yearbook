use chrono::{DateTime, FixedOffset};
use rocket::{
    http::Status,
    serde::{json::Json, Deserialize, Serialize},
};
use std::fmt::Debug;
use utoipa::{ToResponse, ToSchema};

use super::{
    database::DatabaseError,
    prisma::{
        db_expected_user::Data as DbExpectedUser, db_log::Data as DbLog,
        db_other_picture::Data as DbOtherPicture, db_report::Data as DbReport,
        db_user::Data as DbUser, DbLogType, DbProgram, DbReportType, DbUploadStatus,
    },
};

pub type YearbookApiResponse<T> = Result<Json<T>, (Status, Json<YearbookApiError>)>;

/// Generic error for Yearbook API
#[derive(Debug, ToSchema, ToResponse, Deserialize, Serialize, Copy, Clone, Eq, PartialEq)]
pub enum YearbookApiError {
    InternalError,
    Forbidden,
    ProtocolError,
    FormatError,
    NotFound,
    Conflict,
}

impl YearbookApiError {
    /// converts to wrapped response
    pub fn res(self) -> (Status, Json<YearbookApiError>) {
        match self {
            YearbookApiError::InternalError => (Status::InternalServerError, Json(self)),
            YearbookApiError::Forbidden => (Status::Forbidden, Json(self)),
            YearbookApiError::ProtocolError => (Status::BadRequest, Json(self)),
            YearbookApiError::FormatError => (Status::BadRequest, Json(self)),
            YearbookApiError::NotFound => (Status::NotFound, Json(self)),
            YearbookApiError::Conflict => (Status::Conflict, Json(self)),
        }
    }
}

#[derive(Debug, ToSchema, Deserialize, Serialize, Copy, Clone, Eq, PartialEq)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Program {
    MaApp,
    PhApp,
    Ar,
    CgcCh,
    Gc,
    Sc,
    MaComp,
    Cs,
    Cyber,
    Ds,
    Dh,
    El,
    Energ,
    Sie,
    If,
    Sv,
    Mte,
    Mx,
    Ma,
    Gm,
    Mnis,
    Mt,
    CgcMb,
    Nx,
    Ne,
    Siq,
    Ph,
    Robot,
    Stat,
    Smt,
}

impl From<DbProgram> for Program {
    fn from(value: DbProgram) -> Self {
        use Program::*;
        match value {
            DbProgram::MaApp => MaApp,
            DbProgram::PhApp => PhApp,
            DbProgram::Ar => Ar,
            DbProgram::CgcCh => CgcCh,
            DbProgram::Gc => Gc,
            DbProgram::Sc => Sc,
            DbProgram::MaComp => MaComp,
            DbProgram::Cs => Cs,
            DbProgram::Cyber => Cyber,
            DbProgram::Ds => Ds,
            DbProgram::Dh => Dh,
            DbProgram::El => El,
            DbProgram::Energ => Energ,
            DbProgram::Sie => Sie,
            DbProgram::If => If,
            DbProgram::Sv => Sv,
            DbProgram::Mte => Mte,
            DbProgram::Mx => Mx,
            DbProgram::Ma => Ma,
            DbProgram::Gm => Gm,
            DbProgram::Mnis => Mnis,
            DbProgram::Mt => Mt,
            DbProgram::CgcMb => CgcMb,
            DbProgram::Nx => Nx,
            DbProgram::Ne => Ne,
            DbProgram::Siq => Siq,
            DbProgram::Ph => Ph,
            DbProgram::Robot => Robot,
            DbProgram::Stat => Stat,
            DbProgram::Smt => Smt,
        }
    }
}

impl Into<DbProgram> for Program {
    fn into(self) -> DbProgram {
        use DbProgram::*;
        match self {
            Program::MaApp => MaApp,
            Program::PhApp => PhApp,
            Program::Ar => Ar,
            Program::CgcCh => CgcCh,
            Program::Gc => Gc,
            Program::Sc => Sc,
            Program::MaComp => MaComp,
            Program::Cs => Cs,
            Program::Cyber => Cyber,
            Program::Ds => Ds,
            Program::Dh => Dh,
            Program::El => El,
            Program::Energ => Energ,
            Program::Sie => Sie,
            Program::If => If,
            Program::Sv => Sv,
            Program::Mte => Mte,
            Program::Mx => Mx,
            Program::Ma => Ma,
            Program::Gm => Gm,
            Program::Mnis => Mnis,
            Program::Mt => Mt,
            Program::CgcMb => CgcMb,
            Program::Nx => Nx,
            Program::Ne => Ne,
            Program::Siq => Siq,
            Program::Ph => Ph,
            Program::Robot => Robot,
            Program::Stat => Stat,
            Program::Smt => Smt,
        }
    }
}

#[derive(Debug, ToSchema, Serialize, Copy, Clone, Eq, PartialEq)]
pub enum LogType {
    UserNew,
    UserUpdate,
    UserSubmit,
    UserInfo,
    UserWarn,
    UserError,
    FileUpload,
    FileError,
    SystemInfo,
    SystemError,
}

impl From<DbLogType> for LogType {
    fn from(value: DbLogType) -> Self {
        use LogType::*;
        match value {
            DbLogType::UserNew => UserNew,
            DbLogType::UserUpdate => UserUpdate,
            DbLogType::UserSubmit => UserSubmit,
            DbLogType::UserInfo => UserInfo,
            DbLogType::UserWarn => UserWarn,
            DbLogType::UserError => UserError,
            DbLogType::FileUpload => FileUpload,
            DbLogType::FileError => FileError,
            DbLogType::SystemInfo => SystemInfo,
            DbLogType::SystemError => SystemError,
        }
    }
}

impl Into<DbLogType> for LogType {
    fn into(self) -> DbLogType {
        use DbLogType::*;
        match self {
            LogType::UserNew => UserNew,
            LogType::UserUpdate => UserUpdate,
            LogType::UserSubmit => UserSubmit,
            LogType::UserInfo => UserInfo,
            LogType::UserWarn => UserWarn,
            LogType::UserError => UserError,
            LogType::FileUpload => FileUpload,
            LogType::FileError => FileError,
            LogType::SystemInfo => SystemInfo,
            LogType::SystemError => SystemError,
        }
    }
}

#[derive(Clone, ToSchema, Serialize)]
pub struct Log {
    pub id: i32,
    pub timestamp: DateTime<FixedOffset>,
    pub user_id: Option<String>,
    pub message: String,
    pub log_type: LogType,
}

impl From<DbLog> for Log {
    fn from(l: DbLog) -> Self {
        Log {
            id: l.id,
            timestamp: l.timestamp,
            user_id: l.user_id,
            message: l.message,
            log_type: l.log_type.into(),
        }
    }
}

#[derive(ToSchema, Serialize, Clone)]
pub struct User {
    pub sciper: String,
    pub official_firstname: String,
    pub official_lastname: String,
    pub official_program: Program,
    pub preferred_firstname: Option<String>,
    pub preferred_lastname: Option<String>,
    pub preferred_program: Option<Program>,
    pub question_teacher: Option<String>,
    pub session_key: Option<String>,
    pub auth_token: Option<String>,
    pub email: Option<String>,
    pub profile_picture_status: UploadStatus,
    pub profile_picture_meta: Option<ProfilePictureMeta>,
    pub identity_verified: bool,
    pub form_validated: bool,
    pub rejected: bool,
}

impl From<DbUser> for User {
    fn from(user: DbUser) -> Self {
        Self {
            sciper: user.sciper,
            official_firstname: user.official_firstname,
            official_lastname: user.official_lastname,
            official_program: user.official_program.into(),
            preferred_firstname: user.preferred_firstname,
            preferred_lastname: user.preferred_lastname,
            preferred_program: user.preferred_program.map(Program::from),
            question_teacher: user.question_teacher,
            session_key: user.session_key,
            auth_token: user.auth_token,
            email: user.email,
            profile_picture_status: user.profile_picture_status.into(),
            profile_picture_meta: serde_json::from_value(user.profile_picture_meta).ok(),
            identity_verified: user.identity_verified,
            form_validated: user.form_validated,
            rejected: user.rejected,
        }
    }
}

#[derive(ToSchema, Serialize, Deserialize, Debug, Copy, Clone, Eq, PartialEq)]
pub enum UploadStatus {
    Empty,
    ProcessingRembg,
    ProcessingFrame,
    Ready,
    Error,
}

impl From<DbUploadStatus> for UploadStatus {
    fn from(v: DbUploadStatus) -> Self {
        use UploadStatus::*;
        match v {
            DbUploadStatus::Empty => Empty,
            DbUploadStatus::ProcessingRembg => ProcessingRembg,
            DbUploadStatus::ProcessingFrame => ProcessingFrame,
            DbUploadStatus::Ready => Ready,
            DbUploadStatus::Error => Error,
        }
    }
}

impl Into<DbUploadStatus> for UploadStatus {
    fn into(self) -> DbUploadStatus {
        use DbUploadStatus::*;
        match self {
            UploadStatus::Empty => Empty,
            UploadStatus::ProcessingRembg => ProcessingRembg,
            UploadStatus::ProcessingFrame => ProcessingFrame,
            UploadStatus::Ready => Ready,
            UploadStatus::Error => Error,
        }
    }
}

#[derive(ToSchema, Serialize)]
pub struct OtherPicture {
    pub index: i32,
    pub user_id: String,
    pub status: UploadStatus,
}

impl From<DbOtherPicture> for OtherPicture {
    fn from(p: DbOtherPicture) -> Self {
        Self {
            index: p.index,
            user_id: p.user_id,
            status: p.status.into(),
        }
    }
}

#[derive(ToSchema, Serialize)]
pub struct UserData {
    pub user: User,
    pub other_pictures: Vec<OtherPicture>,
}

#[derive(ToSchema, Clone, Debug, Serialize, Deserialize)]
pub struct ProfilePictureMeta {
    pub filename: String,
    pub filesize: i32,
    pub width: i32,
    pub height: i32,
    /// value between 0-255
    pub ae: i32,
    pub frame_specs: ProfilePictureMetaFrameSpecs,
}

impl TryFrom<serde_json::Value> for ProfilePictureMeta {
    type Error = DatabaseError;

    fn try_from(value: serde_json::Value) -> Result<Self, Self::Error> {
        serde_json::from_value(value).map_err(|err| {
            error!("unable to parse picture meta json: {:?}", err);
            DatabaseError::JsonFormat
        })
    }
}

impl TryInto<serde_json::Value> for &ProfilePictureMeta {
    type Error = DatabaseError;

    fn try_into(self) -> Result<serde_json::Value, Self::Error> {
        serde_json::to_value(self).map_err(|err| {
            error!("unable to serialize picture meta json: {:?}", err);
            DatabaseError::Internal
        })
    }
}

#[derive(ToSchema, Clone, Debug, Serialize, Deserialize)]
pub struct ProfilePictureMetaFrameSpecs {
    pub ratio_x: f32,
    pub ratio_y: f32,
    pub ratio_w: f32,
    pub ratio_h: f32,
    pub zoom: i32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProfilePictureMetaFrame {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
}

impl ProfilePictureMetaFrameSpecs {
    pub fn dimensions(&self, image_w: i32, image_h: i32) -> ProfilePictureMetaFrame {
        ProfilePictureMetaFrame {
            x: ((image_w as f64) * (self.ratio_x as f64)).round() as i32,
            y: ((image_h as f64) * (self.ratio_y as f64)).round() as i32,
            w: ((image_w as f64) * (self.ratio_w as f64)).round() as i32,
            h: ((image_h as f64) * (self.ratio_h as f64)).round() as i32,
        }
    }
}

#[derive(ToSchema, Serialize, Clone)]
pub struct ExpectedUser {
    pub sciper: String,
    pub firstname: String,
    pub lastname: String,
    pub program: Program,
    pub mail_epfl: Option<String>,
    pub mail_perso: String,
    pub note: Option<String>,
}

impl From<DbExpectedUser> for ExpectedUser {
    fn from(user: DbExpectedUser) -> Self {
        Self {
            sciper: user.sciper,
            firstname: user.firstname,
            lastname: user.lastname,
            program: user.program.into(),
            mail_epfl: user.mail_epfl,
            mail_perso: user.mail_perso,
            note: user.note,
        }
    }
}

impl Into<DbExpectedUser> for ExpectedUser {
    fn into(self) -> DbExpectedUser {
        DbExpectedUser {
            sciper: self.sciper,
            firstname: self.firstname,
            lastname: self.lastname,
            program: self.program.into(),
            mail_epfl: self.mail_epfl,
            mail_perso: self.mail_perso,
            note: self.note,
        }
    }
}

impl ExpectedUser {
    pub fn join_clone(self, user: Option<&User>) -> ExpectedUserJoined {
        ExpectedUserJoined {
            sciper: self.sciper,
            firstname: self.firstname,
            lastname: self.lastname,
            program: self.program,
            mail_epfl: self.mail_epfl,
            mail_perso: self.mail_perso,
            note: self.note,
            user: user.map(|u| u.to_owned().clone())
        }
    }
}

#[derive(ToSchema, Serialize, Clone)]
pub struct ExpectedUserJoined {
    pub sciper: String,
    pub firstname: String,
    pub lastname: String,
    pub program: Program,
    pub mail_epfl: Option<String>,
    pub mail_perso: String,
    pub note: Option<String>,
    pub user: Option<User>
}

#[derive(ToSchema, Serialize, Deserialize, Debug, Copy, Clone, Eq, PartialEq)]
pub enum ReportType {
    ListError,
    EmailFlagged,
    RembgReport,
}

impl From<DbReportType> for ReportType {
    fn from(v: DbReportType) -> Self {
        use ReportType::*;
        match v {
            DbReportType::ListError => ListError,
            DbReportType::EmailFlagged => EmailFlagged,
            DbReportType::RembgReport => RembgReport,
        }
    }
}

impl Into<DbReportType> for ReportType {
    fn into(self) -> DbReportType {
        use DbReportType::*;
        match self {
            ReportType::ListError => ListError,
            ReportType::EmailFlagged => EmailFlagged,
            ReportType::RembgReport => RembgReport,
        }
    }
}

#[derive(ToSchema, Serialize)]
pub struct Report {
    pub id: i32,
    pub timestamp: DateTime<FixedOffset>,
    pub user_id: Option<String>,
    pub report_type: ReportType,
    pub message: String,
    pub done: bool,
}

impl From<DbReport> for Report {
    fn from(r: DbReport) -> Self {
        Self {
            id: r.id,
            timestamp: r.timestamp,
            user_id: r.user_id,
            report_type: r.report_type.into(),
            message: r.message,
            done: r.done,
        }
    }
}
