use lettre::{
    message::MultiPart, transport::smtp::authentication::Credentials, AsyncSmtpTransport,
    AsyncTransport, Message, Tokio1Executor,
};

pub enum MailingError {
    InternalError,
    AddressParsingError,
}

fn get_auth_url(auth_token: &str) -> String {
    format!(
        "https://yearbook.agepoly.ch/email-callback?auth_token={}",
        auth_token
    )
}

fn email_login_plain(auth_token: &str) -> String {
    format!(
        "Yearbook App\n\nUse the following link to login: {}",
        get_auth_url(auth_token)
    )
}

fn email_login_html(auth_token: &str) -> String {
    include_str!("email_login.html").replace("###REPLACE-WITH-URL###", &get_auth_url(auth_token))
}

pub async fn send_login_mail(user_email: &str, auth_token: &str) -> Result<(), MailingError> {
    let config = crate::config();

    let multipart = MultiPart::alternative_plain_html(
        email_login_plain(auth_token),
        email_login_html(auth_token),
    );

    let email = Message::builder()
        .from(
            config
                .smtp_from
                .parse()
                .expect("unable to parse yearbook From address"),
        )
        .to(user_email
            .parse()
            .map_err(|_| MailingError::AddressParsingError)?)
        .subject("Yearbook - Login with email")
        .multipart(multipart)
        .expect("unable to parse login mail body");

    let sender = AsyncSmtpTransport::<Tokio1Executor>::starttls_relay(&config.smtp_relay)
        .expect("invalid_relay")
        .credentials(Credentials::new(
            config.smtp_user.clone(),
            config.smtp_pass.clone(),
        ))
        .build();

    match sender.send(email).await {
        Ok(res) => {
            debug!("send_login_mail: SMTP response: {:?}", res);
            Ok(())
        }
        Err(err) => {
            error!("send_login_mail: send error: {:?}", err);
            Err(MailingError::InternalError)
        }
    }
}
