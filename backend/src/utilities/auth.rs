use rand::Rng;

use super::database::DatabaseError;
use super::models::LogType;
use super::{database::Database, models::User, tequila::TequilaAttributes};
use crate::config;
use crate::utilities::tequila;

const TEQUILA_DEFAULT_RETURN_URL: &str = "https://yearbook.agepoly.ch/tequila-callback";

pub enum AuthenticationError {
    InternalError,
    TequilaUserError,
    NotFound,
}

/// Creates a new tequila request and returns the authentication URL for the client
pub async fn new_tequila_request() -> Result<String, AuthenticationError> {
    let teq = tequila::Tequila::new();
    let callback_url = match &config().tequila_callback_url {
        Some(url) => {
            debug!("Used Tequila callback url override: {}", url);
            url
        }
        None => TEQUILA_DEFAULT_RETURN_URL,
    };
    let tequila_key: String = teq
        .create_request(callback_url)
        .await
        .map_err(|_| AuthenticationError::InternalError)?;

    Ok(format!("{}{}", tequila::CLIENT_AUTH_URL, tequila_key))
}

pub async fn tequila_callback(
    db: &Database,
    key: &str,
    auth_check: &str,
) -> Result<User, AuthenticationError> {
    let teq = tequila::Tequila::new();
    let values = teq.fetch_attributes(key, auth_check).await.map_err(|err| {
        error!("tequila_callback: fetch_attributes error: {:?}", err);
        AuthenticationError::TequilaUserError
    })?;
    let attributes: TequilaAttributes = values.try_into().map_err(|err| {
        error!("tequila_callback: attributes parsing error: {:?}", err);
        AuthenticationError::InternalError
    })?;

    let user = db
        .auto_register_user(attributes.sciper, attributes.firstname, attributes.name)
        .await
        .map_err(|err| match err {
            DatabaseError::NotFound => AuthenticationError::NotFound,
            _ => AuthenticationError::InternalError,
        })?;

    db.log(
        LogType::UserNew,
        format!("Registered verified user with Tequila: sciper={}", user.sciper.clone()),
        Some(user.sciper.clone()),
    )
    .await;

    Ok(user)
}

pub fn generate_token() -> String {
    rand::thread_rng()
        .sample_iter(rand::distributions::Alphanumeric)
        .take(30)
        .map(char::from)
        .collect()
}
