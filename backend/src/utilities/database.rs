use std::{sync::Arc, vec};

use prisma_client_rust::prisma_errors::query_engine::RecordNotFound;

use super::{
    auth,
    models::{
        ExpectedUser, Log, LogType, ProfilePictureMeta, ProfilePictureMetaFrameSpecs, Program, Report, ReportType, UploadStatus, User, UserData
    },
    prisma::{
        self, db_expected_user, db_log, db_other_picture, db_report, db_user, DbUploadStatus, PrismaClient
    },
    session::UserSession,
};

#[derive(Debug, Clone)]
pub enum DatabaseError {
    NotFound,
    Internal,
    Conflict,
    ImageFormat,
    JsonFormat,
}

#[derive(Clone)]
pub struct Database {
    db: Arc<PrismaClient>,
}

impl Database {
    pub async fn new_client() -> Self {
        let client = prisma::new_client()
            .await
            .expect("Failed to create Prisma client");

        client
            ._migrate_deploy()
            .await
            .expect("Failed to apply Prisma migrations");

        Self {
            db: Arc::new(client),
        }
    }

    pub async fn add_log(&self, log_type: LogType, message: String) -> Result<(), DatabaseError> {
        let _ = self
            .db
            .db_log()
            .create(message, log_type.into(), vec![])
            .exec()
            .await
            .map_err(|_err| DatabaseError::Internal)?;
        Ok(())
    }

    pub async fn get_logs(&self, user: Option<String>) -> Result<Vec<Log>, DatabaseError> {
        let filter = match user {
            Some(user) => vec![db_log::user_id::equals(Some(user))],
            None => vec![]
        };
        let logs = self
            .db
            .db_log()
            .find_many(filter)
            .exec()
            .await
            .map_err(|_err| DatabaseError::Internal)?;
        Ok(logs.into_iter().map(Log::from).collect())
    }

    pub async fn check_expected_user(
        &self,
        sciper: String,
    ) -> Result<Option<ExpectedUser>, DatabaseError> {
        let expected_user = self
            .db
            .db_expected_user()
            .find_unique(db_expected_user::sciper::equals(sciper))
            .exec()
            .await
            .map_err(|err: prisma_client_rust::QueryError| {
                error!("check_expected_user: find_unique error: {:?}", err);
                DatabaseError::Internal
            })?;
        match expected_user {
            Some(u) => Ok(Some(u.into())),
            None => Ok(None),
        }
    }

    /// used exclusively with Tequila login
    pub async fn auto_register_user(
        &self,
        sciper: String,
        official_firstname: String,
        official_lastname: String,
    ) -> Result<User, DatabaseError> {
        let official_program = match self.check_expected_user(sciper.clone()).await? {
            Some(eu) => eu.program,
            None => {
                warn!(
                    "auto_register_user: user={} is not on the list of expected users",
                    sciper
                );
                self.log(
                    LogType::UserError,
                    format!("sciper={} is not on the list of expected users", sciper),
                    None,
                )
                .await;
                return Err(DatabaseError::NotFound);
            }
        };
        let session_key = auth::generate_token();
        let user = self
            .db
            .db_user()
            .upsert(
                db_user::sciper::equals(sciper.clone()),
                db_user::create(
                    sciper.clone(),
                    official_firstname,
                    official_lastname,
                    official_program.into(),
                    true,
                    vec![db_user::session_key::set(Some(session_key.clone()))],
                ),
                vec![
                    db_user::session_key::set(Some(session_key)),
                    db_user::official_program::set(official_program.into()),
                ],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("auto_register_user: upsert error: {:?}", err);
                DatabaseError::Internal
            })?;

        Ok(user.into())
    }

    /// used exclusively with email login
    pub async fn register_unverified_user(
        &self,
        sciper: String,
        email: String,
        new_token: String,
        official_firstname: String,
        official_lastname: String,
        official_program: Program,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_user()
            .create(
                sciper,
                official_firstname,
                official_lastname,
                official_program.into(),
                false,
                vec![
                    db_user::email::set(Some(email)),
                    db_user::auth_token::set(Some(new_token)),
                ],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("register_unverified_user: create error {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(())
    }

    /// used exclusively with email login
    pub async fn update_user_auth_token(
        &self,
        sciper: String,
        email: String,
        new_token: String,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_user()
            .update(
                db_user::sciper::equals(sciper),
                vec![
                    db_user::email::set(Some(email)),
                    db_user::auth_token::set(Some(new_token)),
                ],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("update_user_auth_token: update error {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(())
    }

    /// used exclusively with email login
    pub async fn update_verified_user(&self, auth_token: String) -> Result<User, DatabaseError> {
        let session_key = auth::generate_token();
        let user = self
            .db
            .db_user()
            .update(
                db_user::auth_token::equals(auth_token),
                vec![
                    db_user::auth_token::set(None),
                    db_user::identity_verified::set(true),
                    db_user::session_key::set(Some(session_key)),
                ],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("update_verified_user: update error {:?}", err);
                DatabaseError::Internal
            })?;

        Ok(user.into())
    }

    pub async fn session_from_key(
        &self,
        session_key: String,
    ) -> Result<Option<UserSession>, DatabaseError> {
        let user = self
            .db
            .db_user()
            .find_unique(db_user::session_key::equals(session_key))
            .exec()
            .await
            .map_err(|err| {
                error!("session_from_key: find_unique error: {:?}", err);
                DatabaseError::Internal
            })?;
        match user {
            Some(user) => Ok(Some(UserSession {
                sciper: user.sciper.clone(),
                firstname: user.official_firstname,
                lastname: user.official_lastname,
                profile_picture_status: user.profile_picture_status.into(),
                session_key: user.session_key.ok_or_else(|| {
                    error!(
                        "session_from_key: missing session key for user {}",
                        user.sciper
                    );
                    DatabaseError::Internal
                })?,
            })),
            None => Ok(None),
        }
    }

    pub async fn get_user(&self, sciper: String) -> Result<Option<User>, DatabaseError> {
        let user = self
            .db
            .db_user()
            .find_unique(db_user::sciper::equals(sciper))
            .exec()
            .await
            .map_err(|err| {
                error!("get_user: find_unique error: {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(user.map(|u| u.into()))
    }

    pub async fn get_user_data(&self, sciper: String) -> Result<UserData, DatabaseError> {
        let user = self
            .db
            .db_user()
            .find_unique(db_user::sciper::equals(sciper))
            .with(db_user::other_pictures::fetch(vec![]))
            .exec()
            .await
            .map_err(|err| {
                error!("get_user_data: find_unique error: {:?}", err);
                DatabaseError::Internal
            })?
            .ok_or_else(|| DatabaseError::NotFound)?;
        Ok(UserData {
            user: user.clone().into(),
            other_pictures: user
                .other_pictures
                .unwrap_or_default()
                .into_iter()
                .map(|p| p.into())
                .collect(),
        })
    }

    pub async fn update_user_identity(
        &self,
        sciper: String,
        preferred_firstname: String,
        preferred_lastname: String,
        preferred_program: Option<Program>,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_user()
            .update(
                db_user::sciper::equals(sciper),
                vec![
                    db_user::preferred_firstname::set(Some(preferred_firstname)),
                    db_user::preferred_lastname::set(Some(preferred_lastname)),
                    db_user::preferred_program::set(preferred_program.map(Program::into)),
                ],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("update_user_identity: update error: {:?}", err);
                if err.is_prisma_error::<RecordNotFound>() {
                    DatabaseError::NotFound
                } else {
                    DatabaseError::Internal
                }
            })?;
        Ok(())
    }

    pub async fn update_profile_picture_status(
        &self,
        user: String,
        status: UploadStatus,
    ) -> Result<(), DatabaseError> {
        self.update_profile_picture_status_and_meta(user, status, None)
            .await
    }

    pub async fn update_profile_picture_status_and_get_meta(
        &self,
        user: String,
        status: UploadStatus,
    ) -> Result<Option<ProfilePictureMeta>, DatabaseError> {
        let user_data = self.get_user_data(user.clone()).await?;
        self.update_profile_picture_status_and_meta(user, status, None)
            .await?;
        Ok(user_data.user.profile_picture_meta)
    }

    pub async fn update_profile_picture_status_and_meta(
        &self,
        user: String,
        status: UploadStatus,
        meta: Option<ProfilePictureMeta>,
    ) -> Result<(), DatabaseError> {
        debug!(
            "updating file status: {} -> {:?}, meta={:?}",
            user, status, meta
        );
        let updates = match meta.clone() {
            Some(meta) => {
                let meta_json: serde_json::Value = (&meta).try_into()?;
                vec![
                    db_user::profile_picture_status::set(status.into()),
                    db_user::profile_picture_meta::set(meta_json),
                ]
            }
            None => vec![db_user::profile_picture_status::set(status.into())],
        };
        self.db
            .db_user()
            .update(db_user::sciper::equals(user.clone()), updates)
            .exec()
            .await
            .map_err(|err| {
                error!(
                    "update_profile_picture_status_and_meta: update error: {:?}",
                    err
                );
                if err.is_prisma_error::<RecordNotFound>() {
                    DatabaseError::NotFound
                } else {
                    DatabaseError::Internal
                }
            })?;
        self.log(
            LogType::UserUpdate,
            format!(
                "Updated profile picture status sciper={} status={:?} meta={:?}",
                user.clone(),
                status,
                meta
            ),
            Some(user.clone()),
        )
        .await;
        Ok(())
    }

    pub async fn delete_profile_picture(&self, sciper: String) -> Result<(), DatabaseError> {
        let meta_json = serde_json::json!({});
        self.db
            .db_user()
            .update(
                db_user::sciper::equals(sciper.clone()),
                vec![
                    db_user::profile_picture_status::set(UploadStatus::Empty.into()),
                    db_user::profile_picture_meta::set(meta_json),
                ],
            )
            .exec()
            .await
            .map_err(|err| {
                error!(
                    "update_profile_picture_delete: update user error: {:?}",
                    err
                );
                DatabaseError::Internal
            })?;
        Ok(())
    }

    /// saves the new specs and set the status to PROCESSING
    pub async fn update_profile_picture_frame(
        &self,
        sciper: String,
        new_specs: ProfilePictureMetaFrameSpecs,
    ) -> Result<(ProfilePictureMeta, UploadStatus), DatabaseError> {
        let (tx, db) = self.db._transaction().begin().await.map_err(|err| {
            error!(
                "update_profile_picture_frame: unable to begin transaction: {:?}",
                err
            );
            DatabaseError::Internal
        })?;

        let transaction_block = async {
            // Check the current status & set it to processing
            let user = &db
                .db_user()
                .find_unique(db_user::sciper::equals(sciper.clone()))
                .exec()
                .await
                .map_err(|err| {
                    error!("update_profile_picture_frame: find user error: {:?}", err);
                    DatabaseError::Internal
                })?
                .ok_or_else(|| {
                    error!("update_profile_picture_frame: user={} not found", sciper);
                    DatabaseError::NotFound
                })?;

            let mut meta: ProfilePictureMeta = user.profile_picture_meta.clone().try_into()?;
            meta.frame_specs = new_specs;
            if !super::img_proc::verify_frame_meta(&meta) {
                return Err(DatabaseError::ImageFormat);
            }
            let meta_json: serde_json::Value = (&meta).try_into()?;

            let mut s = user.profile_picture_status;
            if s == DbUploadStatus::ProcessingFrame {
                return Err(DatabaseError::Conflict);
            } else if s == DbUploadStatus::ProcessingRembg {
                // keep the status as is
            } else if s != DbUploadStatus::Ready {
                return Err(DatabaseError::NotFound);
            } else {
                s = DbUploadStatus::ProcessingFrame;
            }

            debug!(
                "updating file status: {} -> {:?}, meta={:?}",
                sciper, s, meta
            );
            db.db_user()
                .update(
                    db_user::sciper::equals(sciper),
                    vec![
                        db_user::profile_picture_status::set(s),
                        db_user::profile_picture_meta::set(meta_json),
                    ],
                )
                .exec()
                .await
                .map_err(|err| {
                    error!("update_profile_picture_frame: update error: {:?}", err);
                    DatabaseError::Internal
                })?;

            Ok((meta, s.into()))
        }
        .await;

        match transaction_block {
            Ok((meta, s)) => {
                tx.commit(db).await.map_err(|err| {
                    error!(
                        "update_profile_picture_frame: unable to commit transaction: {:?}",
                        err
                    );
                    DatabaseError::Internal
                })?;
                Ok((meta, s))
            }
            Err(err) => {
                tx.rollback(db).await.map_err(|err| {
                    error!(
                        "update_profile_picture_frame: unable to rollback transaction: {:?}",
                        err
                    );
                    DatabaseError::Internal
                })?;
                Err(err)
            }
        }
    }

    pub async fn get_users(&self) -> Result<Vec<User>, DatabaseError> {
        let users = self
            .db
            .db_user()
            .find_many(vec![])
            .order_by(db_user::OrderByParam::Sciper(
                prisma_client_rust::Direction::Asc,
            ))
            .exec()
            .await
            .map_err(|err| {
                error!("get_users: find_many error: {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(users.into_iter().map(|u| u.into()).collect())
    }

    pub async fn update_expected_users(
        &self,
        expected_users: Vec<ExpectedUser>,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_expected_user()
            .create_many(
                expected_users
                    .into_iter()
                    .map(|u| {
                        (
                            u.sciper,
                            u.firstname,
                            u.lastname,
                            u.program.into(),
                            u.mail_perso,
                            vec![
                                db_expected_user::mail_epfl::set(u.mail_epfl),
                                db_expected_user::note::set(u.note),
                            ],
                        )
                    })
                    .collect(),
            )
            .skip_duplicates()
            .exec()
            .await
            .map_err(|err| {
                error!("update_expected_users: update_many error: {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(())
    }

    pub async fn get_expected_users(&self) -> Result<Vec<ExpectedUser>, DatabaseError> {
        let users = self
            .db
            .db_expected_user()
            .find_many(vec![])
            .order_by(db_expected_user::OrderByParam::Lastname(
                prisma_client_rust::Direction::Asc,
            ))
            .exec()
            .await
            .map_err(|err| {
                error!("get_expected_users: find_many error: {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(users.into_iter().map(|u| u.into()).collect())
    }

    pub async fn get_expected_user(&self, sciper: String) -> Result<ExpectedUser, DatabaseError> {
        match self
            .db
            .db_expected_user()
            .find_unique(db_expected_user::sciper::equals(sciper))
            .exec()
            .await
        {
            Ok(Some(user)) => Ok(user.into()),
            Ok(None) => Err(DatabaseError::NotFound),
            Err(err) => {
                error!("get_expected_user: find_unique error: {:?}", err);
                Err(DatabaseError::Internal)
            }
        }
    }

    pub async fn update_other_picture(
        &self,
        user: String,
        index: i32,
        status: UploadStatus,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_other_picture()
            .upsert(
                db_other_picture::UniqueWhereParam::UserIdIndexEquals(user.clone(), index),
                (
                    index,
                    db_user::UniqueWhereParam::SciperEquals(user.clone()),
                    vec![db_other_picture::status::set(status.into())],
                ),
                vec![db_other_picture::status::set(status.into())],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("update_other_picture: upsert error: {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(())
    }

    pub async fn update_user_extra(
        &self,
        sciper: String,
        question_teacher: Option<String>,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_user()
            .update(
                db_user::sciper::equals(sciper.clone()),
                vec![db_user::question_teacher::set(question_teacher)],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("update_user_extra: update error: {:?}", err);
                if err.is_prisma_error::<RecordNotFound>() {
                    DatabaseError::NotFound
                } else {
                    DatabaseError::Internal
                }
            })?;
        Ok(())
    }

    pub async fn update_user_form_validated(
        &self,
        sciper: String,
        form_validated: bool,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_user()
            .update(
                db_user::sciper::equals(sciper.clone()),
                vec![db_user::form_validated::set(form_validated)],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("update_user_form_validated: update error: {:?}", err);
                if err.is_prisma_error::<RecordNotFound>() {
                    DatabaseError::NotFound
                } else {
                    DatabaseError::Internal
                }
            })?;
        Ok(())
    }

    pub async fn create_user_report(
        &self,
        sciper: Option<String>,
        report_type: ReportType,
        message: String,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_report()
            .create(
                report_type.into(),
                message,
                vec![db_report::user_id::set(sciper)],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("create_user_report: create error: {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(())
    }

    pub async fn log(&self, log_type: LogType, message: String, user: Option<String>) -> () {
        if let Err(err) = self
            .db
            .db_log()
            .create(
                message.clone(),
                log_type.into(),
                vec![db_log::user_id::set(user.clone())],
            )
            .exec()
            .await
        {
            error!(
                "log: unable to create log (log_type={:?} message={} user={:?}): create error: {:?}",
                log_type, message, user, err
            );
        }
    }

    pub async fn get_reports(&self, sciper: Option<String>) -> Result<Vec<Report>, DatabaseError> {
        let filters = match sciper {
            Some(sciper) => vec![db_report::user_id::equals(Some(sciper))],
            None => vec![]
        };
        let users = self
            .db
            .db_report()
            .find_many(filters)
            .order_by(db_report::OrderByParam::Timestamp(
                prisma_client_rust::Direction::Desc,
            ))
            .exec()
            .await
            .map_err(|err| {
                error!("get_reports: find_many error: {:?}", err);
                DatabaseError::Internal
            })?;
        Ok(users.into_iter().map(|u| u.into()).collect())
    }

    pub async fn update_report_done(
        &self,
        report_id: i32,
        done: bool,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_report()
            .update(
                db_report::id::equals(report_id),
                vec![db_report::done::set(done)],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("update_report_done: update error: {:?}", err);
                if err.is_prisma_error::<RecordNotFound>() {
                    DatabaseError::NotFound
                } else {
                    DatabaseError::Internal
                }
            })?;
        Ok(())
    }

    pub async fn update_user_rejected(
        &self,
        sciper: String,
        rejected: bool,
    ) -> Result<(), DatabaseError> {
        self.db
            .db_user()
            .update(
                db_user::sciper::equals(sciper),
                vec![db_user::rejected::set(rejected)],
            )
            .exec()
            .await
            .map_err(|err| {
                error!("update_user_rejected: update error: {:?}", err);
                if err.is_prisma_error::<RecordNotFound>() {
                    DatabaseError::NotFound
                } else {
                    DatabaseError::Internal
                }
            })?;
        Ok(())
    }
}
