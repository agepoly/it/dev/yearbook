use rocket::fs::{relative, NamedFile};
use rocket::routes;
use rocket::serde::json::Json;
use std::path::{Path, PathBuf};
use utoipa::OpenApi;
use utoipa_rapidoc::RapiDoc;
use utoipa_swagger_ui::SwaggerUi;

#[macro_use]
extern crate rocket;

mod api;
mod utilities;

use crate::utilities::cached_file::CachedFile;
use crate::utilities::config::config;
use crate::utilities::database::Database;

pub type DB = rocket::State<Database>;

#[get("/<file..>")]
async fn static_files(file: PathBuf) -> CachedFile {
    match NamedFile::open(Path::new(relative!("static")).join(file)).await {
        Ok(f) => f,
        Err(_) => NamedFile::open(Path::new(relative!("static/index.html")))
            .await
            .unwrap(),
    }
    .into()
}

#[get("/")]
async fn index() -> CachedFile {
    NamedFile::open(Path::new(relative!("static/index.html")))
        .await
        .unwrap()
        .into()
}

#[get("/api/openapi.json")]
fn openapi() -> Json<utoipa::openapi::OpenApi> {
    Json(api::ApiDoc::openapi())
}

#[launch]
async fn rocket() -> _ {
    config();
    let logger_config = env_logger::Env::default().default_filter_or("yearbook=info,rocket=info");
    env_logger::Builder::from_env(logger_config).init();

    let database = Database::new_client().await;

    rocket::build()
        .manage(database)
        .mount("/", routes![static_files, openapi])
        .mount("/", api::get_routes()) // full paths are used because of utoipa auto discovery
        .mount("/api", RapiDoc::new("/api/openapi.json").path("/rapidoc"))
        .mount(
            "/",
            SwaggerUi::new("/docs/swagger-ui/<_..>")
                .url("/docs/openapi.json", api::ApiDoc::openapi()),
        )
        .mount("/", routes![index])
}
