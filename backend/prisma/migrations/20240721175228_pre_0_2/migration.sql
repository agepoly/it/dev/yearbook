/*
  Warnings:

  - You are about to drop the column `user` on the `logs` table. All the data in the column will be lost.
  - You are about to drop the column `last_step` on the `users` table. All the data in the column will be lost.
  - You are about to drop the `files` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[session_key]` on the table `users` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[email]` on the table `users` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[auth_token]` on the table `users` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `identity_verified` to the `users` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "upload_status" AS ENUM ('EMPTY', 'PROCESSING_REMBG', 'PROCESSING_FRAME', 'READY', 'ERROR');

-- AlterEnum
ALTER TYPE "program" ADD VALUE 'MNIS';

-- DropForeignKey
ALTER TABLE "files" DROP CONSTRAINT "files_uploaded_by_fk";

-- DropForeignKey
ALTER TABLE "logs" DROP CONSTRAINT "logs_user_fk";

-- AlterTable
ALTER TABLE "logs" RENAME CONSTRAINT "logs_pk" TO "logs_pkey";
ALTER TABLE "logs" DROP COLUMN "user",
ADD COLUMN     "userId" VARCHAR(6);

-- AlterTable
ALTER TABLE "users" DROP COLUMN "last_step",
ADD COLUMN     "email" TEXT,
ADD COLUMN     "form_validated" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "identity_verified" BOOLEAN NOT NULL,
ADD COLUMN     "profile_picture_meta" JSONB NOT NULL DEFAULT '{}',
ADD COLUMN     "profile_picture_status" "upload_status" NOT NULL DEFAULT 'EMPTY';

-- DropTable
DROP TABLE "files";

-- DropEnum
DROP TYPE "upload_type";

-- CreateTable
CREATE TABLE "expected_users" (
    "sciper" VARCHAR(6) NOT NULL,
    "firstname" TEXT NOT NULL,
    "lastname" TEXT NOT NULL,
    "program" "program" NOT NULL,
    "mail_epfl" TEXT,
    "mail_perso" TEXT NOT NULL,
    "note" TEXT,

    CONSTRAINT "expected_users_pkey" PRIMARY KEY ("sciper")
);

-- CreateTable
CREATE TABLE "other_pictures" (
    "index" INTEGER NOT NULL,
    "userId" VARCHAR(6) NOT NULL,
    "status" "upload_status" NOT NULL DEFAULT 'EMPTY',

    CONSTRAINT "other_pictures_pkey" PRIMARY KEY ("userId","index")
);

-- CreateIndex
CREATE UNIQUE INDEX "users_session_key_key" ON "users"("session_key");

-- CreateIndex
CREATE UNIQUE INDEX "users_email_key" ON "users"("email");

-- CreateIndex
CREATE UNIQUE INDEX "users_auth_token_key" ON "users"("auth_token");

-- AddForeignKey
ALTER TABLE "other_pictures" ADD CONSTRAINT "other_pictures_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("sciper") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "logs" ADD CONSTRAINT "logs_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("sciper") ON DELETE SET NULL ON UPDATE CASCADE;
