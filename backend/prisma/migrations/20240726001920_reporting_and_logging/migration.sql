-- CreateEnum
CREATE TYPE "report_type" AS ENUM ('LIST_ERROR', 'EMAIL_FLAGGED', 'REMBG_REPORT');

-- CreateTable
CREATE TABLE "reports" (
    "id" SERIAL NOT NULL,
    "timestamp" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userId" VARCHAR(6) NOT NULL,
    "report_type" "report_type" NOT NULL,
    "message" TEXT NOT NULL,
    "done" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "reports_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "reports" ADD CONSTRAINT "reports_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("sciper") ON DELETE RESTRICT ON UPDATE CASCADE;
