-- CreateEnum
CREATE TYPE "log_type" AS ENUM ('USER_NEW', 'USER_UPDATE', 'USER_SUBMIT', 'USER_INFO', 'USER_WARN', 'USER_ERROR', 'FILE_UPLOAD', 'FILE_ERROR', 'SYSTEM_INFO', 'SYSTEM_ERROR');

-- CreateEnum
CREATE TYPE "program" AS ENUM ('MA_APP', 'PH_APP', 'AR', 'CGC_CH', 'GC', 'SC', 'MA_COMP', 'CS', 'CYBER', 'DS', 'DH', 'EL', 'ENERG', 'SIE', 'IF', 'SV', 'MTE', 'MX', 'MA', 'GM', 'MT', 'CGC_MB', 'NX', 'NE', 'SIQ', 'PH', 'ROBOT', 'STAT', 'SMT');

-- CreateEnum
CREATE TYPE "upload_type" AS ENUM ('PROFILE_PIC', 'PROFILE_PIC_PROC', 'OTHER_PIC');

-- CreateTable
CREATE TABLE "files" (
    "id" SERIAL NOT NULL,
    "upload_type" "upload_type" NOT NULL,
    "uploaded_at" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "uploaded_by" VARCHAR(6) NOT NULL,
    "metadata" JSON NOT NULL DEFAULT '{}',

    CONSTRAINT "files_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "logs" (
    "id" SERIAL NOT NULL,
    "timestamp" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "user" VARCHAR(6),
    "message" TEXT NOT NULL,
    "log_type" "log_type" NOT NULL,

    CONSTRAINT "logs_pk" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "users" (
    "sciper" VARCHAR(6) NOT NULL,
    "official_firstname" TEXT NOT NULL,
    "official_lastname" TEXT NOT NULL,
    "official_program" "program" NOT NULL,
    "preferred_firstname" TEXT,
    "preferred_lastname" TEXT,
    "preferred_program" "program",
    "question_teacher" TEXT,
    "last_step" INTEGER NOT NULL,
    "session_key" TEXT,
    "auth_token" TEXT,

    CONSTRAINT "users_pkey" PRIMARY KEY ("sciper")
);

-- AddForeignKey
ALTER TABLE "files" ADD CONSTRAINT "files_uploaded_by_fk" FOREIGN KEY ("uploaded_by") REFERENCES "users"("sciper") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "logs" ADD CONSTRAINT "logs_user_fk" FOREIGN KEY ("user") REFERENCES "users"("sciper") ON DELETE SET NULL ON UPDATE CASCADE;
