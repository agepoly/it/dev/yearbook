import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueDevTools from 'vite-plugin-vue-devtools'
import VueI18nPlugin from '@intlify/unplugin-vue-i18n'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    VueI18nPlugin.vite({
      include: [path.resolve(__dirname, './src/locales/**')]
    }),
    vue(),
    vueDevTools()
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    port: 5000,
    host: '0.0.0.0',
    proxy: {
      '^/api': {
        ws: false,
        changeOrigin: true,
        target: 'http://localhost:4000',
        secure: false
      }
    }
  }
})
