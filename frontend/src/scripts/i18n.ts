import { createI18n } from 'vue-i18n'

import messages from '@intlify/unplugin-vue-i18n/messages'

export const i18nInstance = createI18n({
  legacy: false,
  locale: 'fr',
  messages,
  fallbackLocale: 'en'
})
export const i18n = i18nInstance.global
