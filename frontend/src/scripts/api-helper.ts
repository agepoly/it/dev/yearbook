import createClient from 'openapi-fetch'
import type { paths, components } from '@/openapi'
import { useAppStore } from '@/stores/app'

export let client = initClient('client-not-ready')
let lastBaseUrl: string | null = null

export type User = components['schemas']['User']
export type Program = components['schemas']['Program']
export type UserData = components['schemas']['UserData']
export type OtherPicture = components['schemas']['OtherPicture']
export type UploadStatus = components['schemas']['UploadStatus']
export type ProfilePictureMeta = components['schemas']['ProfilePictureMeta']
export type ProfilePictureMetaFrameSpecs = components['schemas']['ProfilePictureMetaFrameSpecs']
export type ExpectedUserJoined = components['schemas']['ExpectedUserJoined']
export type PostEmailLoginResponse = components['schemas']['PostEmailLoginResponse']
export type Report = components['schemas']['Report']
export type Log = components['schemas']['Log']
export type ReportType = components['schemas']['ReportType']

function initClient(baseUrl: string, sessionKey?: string) {
  const c = createClient<paths>({
    baseUrl,
    headers: {
      Authorization: sessionKey ? `Bearer ${sessionKey}` : null
    }
  })

  c.use({
    onResponse({ response }) {
      if (response.status === 403) {
        console.log('403 AUTH FAILURE')
        const app = useAppStore()
        app.handleAuthFailure()
      }
      return response
    }
  })

  return c
}

export function configClient(baseUrl?: string, sessionKey?: string) {
  client = initClient(baseUrl || lastBaseUrl || 'client-not-ready', sessionKey || undefined)
  lastBaseUrl = baseUrl || lastBaseUrl
}
