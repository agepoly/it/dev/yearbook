import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import { i18nInstance } from './scripts/i18n'

import '@/assets/base.css'

const app = createApp(App)

app.use(i18nInstance)
app.use(createPinia())
app.use(router)

import AppButton from '@/components/ui/AppButton.vue'
import AppCard from '@/components/ui/AppCard.vue'

app.component('AppButton', AppButton)
app.component('AppCard', AppCard)

app.mount('#app')
