<script setup lang="ts">
import { onMounted, reactive } from 'vue'
import { Modal } from 'flowbite'
import { useAppStore } from '@/stores/app'
import type { ReportType } from '@/scripts/api-helper'

let modal: Modal | null = null
const state = reactive({
  reportType: null as ReportType | null,
  reportContext: null as any | null,
  message: null as string | null,
  loading: false,
  success: false
})

const app = useAppStore()
app.$subscribe(() => {
  const globalReportType = !!app.globalReport
  if (modal!.isVisible() !== globalReportType) {
    if (globalReportType) {
      state.reportType = app.globalReport
      state.reportContext = app.globalReportContext
      state.message = ''
      modal!.show()
    } else {
      modal!.hide()
    }
  }
})

onMounted(() => {
  const $modalElement: HTMLElement | null = document.querySelector('#report-modal')

  const modalOptions = {
    backdropClasses: 'bg-gray-900/50 dark:bg-gray-900/80 fixed inset-0 z-40'
  }

  if ($modalElement) {
    modal = new Modal($modalElement, modalOptions)
    modal.updateOnHide(() => app.setGlobalReport(null))
  }
})

function hideModal() {
  app.setGlobalReport(null)
  modal!.hide()
}

function submit() {
  if (state.loading || state.reportType == null || state.message == null) {
    return
  }
  state.loading = true
  switch (state.reportType) {
    case 'ListError':
      if (
        typeof state.reportContext.sciper !== 'string' ||
        typeof state.reportContext.email !== 'string'
      ) {
        throw 'sciper or email missing in report context'
      }
      app
        .reportListError(state.reportContext.sciper, state.reportContext.email, state.message)
        .then((success) => {
          state.loading = false
          state.success = success
        })
      break
    case 'RembgReport':
      app.reportRembg(state.message).then((success) => {
        state.loading = false
        state.success = success
      })
      break
    default:
      throw `unknown report type ${state.reportType}`
  }
}
</script>

<template>
  <div
    id="report-modal"
    tabindex="-1"
    class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full"
  >
    <div class="relative p-4 w-full max-w-md max-h-full">
      <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
        <button
          type="button"
          class="absolute top-3 end-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
          @click="hideModal"
        >
          <svg
            class="w-3 h-3"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 14"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
            />
          </svg>
        </button>
        <div class="p-4 md:p-5">
          <div class="mb-4 flex">
            <svg
              class="w-6 h-6 me-1 text-gray-400 dark:text-gray-500"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              fill="currentColor"
              viewBox="0 0 24 24"
            >
              <path
                d="M13.09 3.294c1.924.95 3.422 1.69 5.472.692a1 1 0 0 1 1.438.9v9.54a1 1 0 0 1-.562.9c-2.981 1.45-5.382.24-7.25-.701a38.739 38.739 0 0 0-.622-.31c-1.033-.497-1.887-.812-2.756-.77-.76.036-1.672.357-2.81 1.396V21a1 1 0 1 1-2 0V4.971a1 1 0 0 1 .297-.71c1.522-1.506 2.967-2.185 4.417-2.255 1.407-.068 2.653.453 3.72.967.225.108.443.216.655.32Z"
              />
            </svg>

            <h3 class="text-lg sm:text-xl font-bold text-gray-900 dark:text-white">
              {{ $t('modalReport.title') }}
            </h3>
          </div>

          <form v-if="!state.success" submit:prevent class="space-y-2">
            <div>
              <label
                for="report-type"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >{{ $t('modalReport.labelReportType') }}</label
              >

              <input
                type="text"
                id="report-type"
                class="opacity-80 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                :value="
                  state.reportType === 'ListError'
                    ? $t('modalReport.typeListError')
                    : $t('modalReport.typeRembg')
                "
                disabled
                required
              />
            </div>
            <div>
              <label
                for="report-message"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >{{ $t('modalReport.labelComment') }}</label
              >

              <textarea
                id="report-message"
                v-model="state.message"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required
              />
            </div>
          </form>
          <div v-else class="relative p-4 sm:p-5 text-center">
            <div
              class="mt-3 w-12 h-12 rounded-full bg-green-100 dark:bg-green-900 p-2 flex items-center justify-center mx-auto mb-3.5"
            >
              <svg
                aria-hidden="true"
                class="w-8 h-8 text-green-500 dark:text-green-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </div>
            <p class="mb-4 text-lg font-semibold text-gray-900 dark:text-white">
              {{ $t('modalReport.success') }}
            </p>
          </div>

          <!-- Modal footer -->
          <div v-if="!state.success" class="mt-6 flex justify-between">
            <AppButton variant="text-faint" @click="hideModal">{{ $t('ui.cancel') }}</AppButton>
            <AppButton @click="submit" :loading="state.loading">{{ $t('ui.submit') }}</AppButton>
          </div>
          <div v-else class="flex flex-row-reverse">
            <AppButton @click="hideModal">{{ $t('ui.close') }}</AppButton>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>
