import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import FormIdentity from '@/views/FormIdentitity.vue'
import FormEntrypoint from '@/views/FormEntrypoint.vue'
import FormDone from '@/views/FormDone.vue'
import FormProfilePicture from '@/views/FormProfilePicture.vue'
import FormExtra from '@/views/FormExtra.vue'
import { useAppStore } from '@/stores/app'
import LoadingView from '@/views/LoadingView.vue'
import ErrorPage from '@/views/ErrorPage.vue'
import YearbookForm from '@/views/YearbookForm.vue'
import EmailLogin from '@/views/EmailLogin.vue'

function checkActiveSession() {
  const app = useAppStore()
  return app.isSessionActive ? true : '/'
}

function checkAdminKey() {
  const app = useAppStore()
  return app.isAdmin ? true : '/'
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'index',
      component: HomeView
    },
    {
      path: '/home',
      name: 'home',
      redirect: () => {
        const app = useAppStore()
        return app.isSessionActive ? '/form' : '/'
      }
    },
    {
      path: '/form',
      name: 'form',
      component: YearbookForm,
      beforeEnter: () => checkActiveSession(),
      children: [
        {
          path: '',
          name: 'form-entrypoint',
          component: FormEntrypoint
        },
        {
          path: 'identity',
          name: 'form-identity',
          component: FormIdentity
        },
        {
          path: 'profile-picture',
          name: 'form-profile-picture',
          component: FormProfilePicture
        },
        {
          path: 'extra',
          name: 'form-extra',
          component: FormExtra
        },
        {
          path: 'done',
          name: 'form-done',
          component: FormDone
        }
      ]
    },
    {
      path: '/admin',
      component: () => import('@/views/admin/Admin.vue'),
      children: [
        {
          path: '',
          component: () => import('@/views/admin/AdminHome.vue')
        },
        {
          path: 'expected-users',
          component: () => import('@/views/admin/AdminExpectedUsers.vue')
        },
        {
          path: 'users',
          component: () => import('@/views/admin/AdminUsers.vue')
        },
        {
          path: 'reports',
          component: () => import('@/views/admin/AdminReports.vue')
        },
        {
          path: 'files',
          component: () => import('@/views/admin/AdminFiles.vue')
        },
        {
          path: 'user/:sciper',
          component: () => import('@/views/admin/AdminUserDetails.vue')
        }
        // {
        //   path: 'register-user/:sciper',
        //   component: () => import('@/views/admin/AdminRegisterUser.vue')
        // }
      ]
    },
    {
      path: '/email-login',
      component: EmailLogin
    },
    {
      path: '/email-callback',
      component: LoadingView,
      name: 'email-callback'
    },
    {
      path: '/tabassage-de-boules',
      component: () => import('@/views/Memory.vue')
    },
    {
      path: '/tequila-callback',
      name: 'tequila-callback',
      component: LoadingView
    },
    {
      path: '/tequila-list-error',
      name: 'tequila-list-error',
      component: ErrorPage
    },
    {
      path: '/logout',
      name: 'logout',
      redirect() {
        const app = useAppStore()
        app.clearSessionKey()
        return '/home'
      }
    },
    {
      path: '/en',
      name: 'lang-en',
      redirect() {
        const app = useAppStore()
        app.setLocale('en')
        return '/home'
      }
    },
    {
      path: '/fr',
      name: 'lang-fr',
      redirect() {
        const app = useAppStore()
        app.setLocale('fr')
        return '/home'
      }
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'not-found',
      component: ErrorPage
    }
  ]
})

export default router
