import { defineStore } from 'pinia'
import {
  client,
  configClient,
  type PostEmailLoginResponse,
  type ReportType
} from '@/scripts/api-helper'
import { i18n } from '@/scripts/i18n'
import { useMyDataStore } from '@/stores/my-data'

function setDocumentDarkMode(darkMode: boolean) {
  if (darkMode) {
    document.documentElement.classList.add('dark')
  } else {
    document.documentElement.classList.remove('dark')
  }
}

export enum GlobalError {
  InternalServerError = 'errors.internal',
  TequilaUserError = 'errors.tequila',
  ActionFailedError = 'errors.actionFailed',
  AuthenticationFailed = 'errors.authenticationFailed',
  MailLoginUnavailable = 'errors.mailLoginUnavailable',
  ProfilePictureBadFormat = 'errors.profilePictureBadFormat',
  UploadTooLarge = 'errors.uploadTooLarge',
  InvalidSubmit = 'errors.invalidSubmit'
}

export function globalErrorMessage(error: GlobalError): string {
  return i18n.t(error.toString())
}

export const useAppStore = defineStore('app', {
  state: () => {
    const savedLocale = localStorage.getItem('locale') as 'fr' | 'en'
    const locale = savedLocale || 'fr'

    const savedColorTheme = localStorage.getItem('color-theme')
    const darkMode =
      savedColorTheme === 'dark' ||
      (!('color-theme' in localStorage) &&
        window.matchMedia('(prefers-color-scheme: dark)').matches)
    setDocumentDarkMode(darkMode)

    const devMode = import.meta.env.MODE === 'development'
    const defaultBaseUrl = devMode ? '/' : 'https://yearbook.agepoly.ch'
    if (devMode) {
      console.log('DEV MODE ENABLED')
    }

    const sessionKey = localStorage.getItem('session')
    configClient(defaultBaseUrl, sessionKey as string | undefined)
    const adminKey = localStorage.getItem('admin')

    const globalError = null as GlobalError | null
    const globalReport = null as ReportType | null
    const globalReportContext = null as any | null

    return {
      locale,
      darkMode,
      sessionKey,
      adminKey,
      globalError,
      globalReport,
      globalReportContext
    }
  },
  getters: {
    isSessionActive: (state) => !!state.sessionKey,
    isAdmin: (state) => !!state.adminKey
  },
  actions: {
    setLocale(locale: 'fr' | 'en') {
      this.locale = locale
      localStorage.setItem('locale', locale)
    },
    toggleDarkMode() {
      this.darkMode = !this.darkMode
      setDocumentDarkMode(this.darkMode)
      localStorage.setItem('color-theme', this.darkMode ? 'dark' : 'light')
    },
    setGlobalError(message: GlobalError | null) {
      this.globalError = message
    },
    setGlobalReport(reportType: ReportType | null, context?: any) {
      this.globalReport = reportType
      this.globalReportContext = context ?? null
    },
    setSessionKey(sessionKey: string) {
      this.sessionKey = sessionKey
      configClient(undefined, sessionKey)
      localStorage.setItem('session', sessionKey)
    },
    clearSessionKey() {
      this.sessionKey = null
      configClient(undefined)
      localStorage.removeItem('session')
      const myData = useMyDataStore()
      myData.clear()
    },
    // Same as clear session key but user is routed to home with an error message (App.vue)
    handleAuthFailure() {
      this.clearSessionKey()
    },
    setAdminKey(adminKey: string) {
      this.adminKey = adminKey
      localStorage.setItem('admin', adminKey)
    },
    clearAdminKey() {
      this.adminKey = null
      localStorage.removeItem('admin')
    },

    async tequilaRequest(): Promise<string | undefined> {
      const { data, error } = await client.POST('/api/login/tequila-request')
      if (error != null) {
        this.setGlobalError(GlobalError.InternalServerError)
        return
      }
      console.log(data, error)
      return data.auth_url
    },

    async tequilaCallback(
      requestKey: string,
      authCheck: string
    ): Promise<'ok' | 'list-error' | 'other-error'> {
      const { data, error } = await client.POST('/api/login/tequila-callback', {
        body: {
          request_key: requestKey,
          auth_check: authCheck
        }
      })
      if (error === 'ProtocolError') {
        this.setGlobalError(GlobalError.TequilaUserError)
        return 'other-error'
      } else if (error === 'NotFound') {
        return 'list-error'
      } else if (error != null) {
        this.setGlobalError(GlobalError.InternalServerError)
        return 'other-error'
      } else {
        this.setSessionKey(data.session_key)
        return 'ok'
      }
    },

    async emailLoginRequest(
      email: string,
      sciper: string,
      ignoreEmailWarning: boolean
    ): Promise<PostEmailLoginResponse | null> {
      const { data, error } = await client.POST('/api/login/email-login-request', {
        body: {
          email,
          sciper,
          ignore_email_warning: ignoreEmailWarning
        }
      })
      if (error === 'Conflict') {
        this.setGlobalError(GlobalError.MailLoginUnavailable)
        return null
      } else if (error != null) {
        this.setGlobalError(GlobalError.InternalServerError)
        return null
      } else {
        return data
      }
    },

    async emailLoginCallback(authToken: string): Promise<void> {
      const { data, error } = await client.POST('/api/login/email-login-callback', {
        body: {
          auth_token: authToken
        }
      })
      if (error === 'NotFound') {
        this.setGlobalError(GlobalError.AuthenticationFailed)
      } else if (error != null) {
        this.setGlobalError(GlobalError.InternalServerError)
      } else {
        this.setSessionKey(data.session_key)
      }
    },

    async reportListError(sciper: string, email: string, message: string): Promise<boolean> {
      const { error } = await client.POST('/api/user/report/list-error', {
        body: {
          sciper,
          email,
          message
        }
      })
      if (error != null) {
        const app = useAppStore()
        console.error(error)
        app.setGlobalError(GlobalError.ActionFailedError)
        return false
      }
      return true
    },

    async reportRembg(message: string): Promise<boolean> {
      const { error } = await client.POST('/api/user/report/rembg', {
        body: {
          message
        }
      })
      if (error != null) {
        const app = useAppStore()
        console.error(error)
        app.setGlobalError(GlobalError.ActionFailedError)
        return false
      }
      return true
    }
  }
})
