import { defineStore } from 'pinia'
import {
  type User,
  type UploadStatus,
  client,
  type ProfilePictureMetaFrameSpecs,
  type Program,
  type OtherPicture
} from '@/scripts/api-helper'
import { GlobalError, useAppStore } from '@/stores/app'

export const useMyDataStore = defineStore('my-data', {
  state: () => {
    return {
      loading: false,
      user: null as null | User,
      other_pictures: null as null | OtherPicture[]
    }
  },

  getters: {
    isLoaded: (state) => !state.loading && state.user != null
  },

  actions: {
    clear() {
      this.loading = false
      this.user = null
      this.other_pictures = null
    },

    async load(force?: boolean, silent?: boolean) {
      const app = useAppStore()
      if (!app.isSessionActive) {
        throw Error('unauthenticated')
      }
      if (this.loading || (!force && this.isLoaded)) {
        return
      }
      if (!silent) {
        this.loading = true
      }
      const { data, error } = await client.GET('/api/user/data')
      if (error != null) {
        this.loading = false
        return
      }
      this.user = data.user
      this.other_pictures = data.other_pictures
      this.loading = false
    },

    async updateIdentity(firstname: string, lastname: string, program: Program): Promise<void> {
      const { error } = await client.POST('/api/user/identity', {
        body: {
          preferred_firstname: firstname,
          preferred_lastname: lastname,
          preferred_program: program
        }
      })
      if (error != null) {
        const app = useAppStore()
        app.setGlobalError(GlobalError.ActionFailedError)
        return
      }
      this.load(true, true)
    },

    async getProfilePicture(): Promise<{
      status: UploadStatus
      originalUrl: string | null
      processedUrl: string | null
    } | null> {
      const { data: status, error } = await client.GET('/api/user/profile-picture-status')
      if (error != null) {
        const app = useAppStore()
        console.error(error)
        app.setGlobalError(GlobalError.ActionFailedError)
        return null
      }
      const app = useAppStore()
      const r = Date.now()

      let originalUrl = null
      if (status !== 'Empty' && status !== 'Error') {
        originalUrl = `/api/user/profile-picture?key=${app.sessionKey}&r=${r}`
      }

      let processedUrl = null
      if (status === 'Ready') {
        processedUrl = `/api/user/profile-picture?key=${app.sessionKey}&cropped&r=${r}`
        this.load(true, true)
      }

      this.user!.profile_picture_status = status
      return { status, originalUrl, processedUrl }
    },

    async deleteProfilePicture(): Promise<void> {
      const { error } = await client.DELETE('/api/user/profile-picture')
      if (error != null) {
        const app = useAppStore()
        console.error(error)
        app.setGlobalError(GlobalError.ActionFailedError)
      }

      // reload user in any case
      this.load(true)
    },

    async updateProfilePictureFrameSpecs(specs: ProfilePictureMetaFrameSpecs): Promise<void> {
      const { error } = await client.POST('/api/user/profile-picture/frame', {
        body: {
          frame_specs: specs
        }
      })
      if (error != null) {
        if (error === 'Conflict') {
          throw Error('conflict')
        }

        const app = useAppStore()
        console.error(error)
        app.setGlobalError(GlobalError.ActionFailedError)
      } else {
        this.load(true, true)
      }
    },

    async deleteOtherPicture(index: number): Promise<void> {
      const { error } = await client.DELETE('/api/user/other-picture/{index}', {
        params: {
          path: {
            index
          }
        }
      })
      if (error != null) {
        const app = useAppStore()
        console.error(error)
        app.setGlobalError(GlobalError.ActionFailedError)
      } else {
        this.load(true, true)
      }
    },

    async getOtherPictures(): Promise<({ index: number; url: string } | null)[]> {
      await this.load(true, true)
      if (this.other_pictures == null) {
        return []
      }
      const app = useAppStore()
      const arr = []
      for (let i = 0; i < this.other_pictures.length; i++) {
        const p = this.other_pictures.find((p) => p.index === i && p.status === 'Ready')
        if (p != null) {
          arr.push({
            index: p.index,
            url: `/api/user/other-picture/${p.index}?key=${app.sessionKey}&r=${Date.now()}`
          })
        } else {
          arr.push(null)
        }
      }
      return arr
    },

    async updateExtra(questionTeacher: string): Promise<void> {
      const { error } = await client.POST('/api/user/extra', {
        body: {
          question_teacher: questionTeacher.length > 0 ? questionTeacher : null
        }
      })
      if (error != null) {
        const app = useAppStore()
        console.error(error)
        app.setGlobalError(GlobalError.ActionFailedError)
      }
      this.load(true, true)
    },

    async confirm(): Promise<boolean> {
      const { error } = await client.POST('/api/user/confirm', {
        body: {}
      })
      if (error != null) {
        const app = useAppStore()
        console.error(error)
        app.setGlobalError(GlobalError.InvalidSubmit)
        return false
      }
      this.load(true)
      return true
    }
  }
})
