/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}', './node_modules/flowbite/**/*.js'],
  theme: {
    extend: {
      screens: {
        xs: '420px'
      }
    }
  },
  plugins: [require('flowbite/plugin')]
}
